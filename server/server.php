<?php

/**
 * server.php
 * 
 * Application web server script
 * -> long-running process akin to a server in Node.JS
 * 
 * @author Lochemem Bruno Michael <lochbm@gmail.com>
 */

declare(strict_types=1);

require __DIR__ . '/vendor/autoload.php';

use React\EventLoop\Factory;
use function App\Router\{loadConfig, app};
use Sikei\React\Http\Middleware\{
  ResponseCompressionMiddleware,
  CompressionDeflateHandler,
  CompressionGzipHandler,
};
use WyriHaximus\React\Http\Middleware\CustomRequestBodyParsers;

// load .env configuration
$config = loadConfig(__DIR__);

// instantiate the event loop
$loop   = Factory::create();

// pass middleware and configuration to app routines
// -> propagate configuration via Reader monad's run() funciton
$app    = app(
  $loop,
  App\Router\internalErrorMiddleware,
  new ResponseCompressionMiddleware([
    new CompressionDeflateHandler(),
    new CompressionGzipHandler(),
  ]),
  new CustomRequestBodyParsers(),
)->run($config);

$loop->run();
