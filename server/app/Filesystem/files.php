<?php

/**
 * Filesystem/files.php
 * 
 * Contains functions for asynchronous filesystem interactions
 * 
 * @author Lochemem Bruno Michael <lochbm@gmail.com>
 */

declare(strict_types=1);

namespace App\Filesystem;

use \React\{
  Filesystem\Node\File,
  EventLoop\LoopInterface,
  Promise\PromiseInterface,
  Filesystem\Filesystem as Rfsys,
};
use \Ergebnis\Json\{
  Printer,
  Normalizer,
  Normalizer\Json,
  Normalizer\Format\Indent,
  Normalizer\ChainNormalizer,
  Normalizer\IndentNormalizer,
  Normalizer\JsonEncodeNormalizer,
  Normalizer\Format\JsonEncodeOptions,
};
use \Chemem\Bingo\Functional\Algorithms as f;
use function \React\Promise\reject;

/**
 * fsysInit
 * creates unique React File instance
 * 
 * fsysInit :: Object -> String -> Object
 *
 * @param LoopInterface $loop
 * @param string $path
 * @return File
 * @example
 * 
 * fsysInit($loop, 'path/to/file')
 * //=> object(React\Filesystem\Node\File) {}
 */
function fsysInit(LoopInterface $loop, string $path)
{
  return Rfsys::create($loop)
    ->file($path);
}

const fsysInit = __NAMESPACE__ . '\\fsysInit';

/**
 * fileRead
 * asynchronously read file contents
 *
 * fileRead :: Object -> String -> Promise s a
 * 
 * @param LoopInterface $loop
 * @param string $file
 * @return PromiseInterface
 * @example
 * 
 * fileRead($loop, '/path/to/file')
 * //=> object(React\Promise\Promise) {}
 */
function fileRead(LoopInterface $loop, string $file): PromiseInterface
{
  return \is_file($file) ?
    fsysInit($loop, $file)->getContents() :
    reject(new \Exception('Invalid file'));
}

const fileRead = __NAMESPACE__ . '\\fileRead';

/**
 * fileWrite
 * asynchronously write data to file
 *
 * fileWrite :: Object -> String -> String -> Promise s a
 * 
 * @param LoopInterface $loop
 * @param string $file
 * @param string $contents
 * @return PromiseInterface
 * 
 * fileWrite()
 * //=>
 */
function fileWrite(
  LoopInterface $loop,
  string $file,
  string $contents = ''
): PromiseInterface {
  return \is_file($file) ?
    fsysInit($loop, $file)->putContents($contents) :
    reject(new \Exception('Invalid file'));
}

const fileWrite = __NAMESPACE__ . '\\fileWrite';

/**
 * fileAppend
 * asynchronously appends contents to a file
 * -> explicitly appends contents to a file
 *
 * fileAppend :: Object -> String -> (String -> String) -> Promise s a
 * 
 * @param LoopInterface $loop
 * @param string $file
 * @param callable $append
 * @return PromiseInterface
 * @example
 * 
 * fileAppend($loop, '/path/to/file', f\identity)
 * //=>
 */
function fileAppend(
  LoopInterface $loop,
  string $file,
  callable $append
): PromiseInterface {
  return fileRead($loop, $file)
      ->then(
        // append contents to a file via function
        fn (string $contents) =>
          fileWrite($loop, $file, $append($contents)),
      );
}

const fileAppend = __NAMESPACE__ . '\\fileAppend';

/**
 * databasePath
 * prints absolute path to database file extracted from configuration
 *
 * databasePath :: String -> String -> Array -> String
 * 
 * @internal
 * @param string $dbname
 * @param string $default
 * @param array $config
 * @return string
 * @example
 * 
 * databasePath('USERS_DB', 'users.json', [
 *  'USERS_DB' => 'auth_users.json',
 * ])
 * //=> '/path/to/project/storage/auth_users.json'
 */
function databasePath(
  string $dbname,
  string $default = '',
  array $config   = []
): string {
  return f\filePath(0, 'storage', f\pluck($config, $dbname, $default));
}

const databasePath = __NAMESPACE__ . '\\databasePath';

/**
 * jsonEncode
 * encodes PHP hashtable as specially formatted JSON
 * 
 * jsonEncode :: Array -> String
 * 
 * @internal
 * @param array $data
 * @return string
 * @example
 * 
 * jsonEncode(['foo' => 'bar'])
 * //=> 
 * {
 *  "foo": "bar"
 * }
 */
function jsonEncode(array $data): string
{
  $json = Json::fromEncoded(
    \json_encode($data, JSON_PRETTY_PRINT)
  );

  return (string) (new ChainNormalizer(
    new JsonEncodeNormalizer(
      JsonEncodeOptions::fromInt(
        JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
      ),
    ),
    // set JSON indentation to 2 spaces
    new IndentNormalizer(
      Indent::fromString('  '),
      new Printer\Printer(),
    ),
  ))->normalize($json);
}

const jsonEncode = __NAMESPACE__ . '\\jsonEncode';
