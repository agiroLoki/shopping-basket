<?php

declare(strict_types=1);

namespace App\Tests\Filesystem;

use App\Filesystem as fsys;
use \React\Filesystem\Node\File;
use \Chemem\Bingo\Functional\Algorithms as f;

class FilesTest extends \seregazhuk\React\PromiseTesting\TestCase
{
  public function fsysInitProvider(): array
  {
    return [
      ['foo.txt'],
      [f\filePath(0, 'composer.json')],
    ];
  }

  /**
   * @dataProvider fsysInitProvider
   */
  public function testfsysInitCreatesUniqueReactFileInstance($path): void
  {
    $fsys = fsys\fsysInit($this->eventLoop(), $path);

    $this->assertInstanceOf(File::class, $fsys);
  }
  
  public function fileReadProvider(): array
  {
    return [
      [__DIR__ . '/files/read.txt', 'TEST FILE'],
      [f\filePath(0, 'foo.txt'), 'Invalid file'],
    ];
  }

  /**
   * @dataProvider fileReadProvider
   */
  public function testfileReadAsynchronouslyReadsFileContents($file, $result): void
  {
    $contents = $this->waitForPromise(
      fsys\fileRead($this->eventLoop(), $file)
        ->then(null, fn ($err) => $err->getMessage()),
      (int) $GLOBALS['timeout'],
    );

    $this->assertEquals($result, $contents);
  }

  public function fileWriteProvider(): array
  {
    return [
      [__DIR__ . '/files/write.txt', 'foo-bar', null],
      [__DIR__ . '/write.txt', '1, 2, 3, 9', 'Invalid file'],
    ];
  }

  /**
   * @dataProvider fileWriteProvider
   */
  public function testfileWriteAsynchronouslyWritesDataToFile($file, $contents, $result): void
  {
    $write = $this->waitForPromise(
      fsys\fileWrite($this->eventLoop(), $file, $contents)
        ->then(null, fn ($err) => $err->getMessage()),
      (int) $GLOBALS['timeout'],
    );

    $this->assertEquals($result, $write);
  }

  public function fileAppendProvider(): array
  {
    return [
      [__DIR__ . '/files/append.txt', f\partial(f\concat, ',', 'foo')],
      [__DIR__ . '/append.txt', f\identity],
    ];
  }

  /**
   * @dataProvider fileAppendProvider
   */
  public function testfileAppendAsynchronouslyAppendsDataToFile($file, $func): void
  {
    $result = $this->waitForPromise(
      fsys\fileAppend($this->eventLoop(), $file, $func)
        ->then(null, fn ($err) => $err->getMessage()),
      (int) $GLOBALS['timeout'],
    );

    $this->assertTrue(\is_null($result) || \is_string($result));
  }

  function databasePathProvider(): array
  {
    $path = f\partial(f\filePath, 0, 'storage');

    return [
      [['DB_USERS', 'db_users.json', []], $path('db_users.json')],
      [['USERS_DB', 'users.json', ['USERS_DB' => 'users.json']], $path('users.json')],
    ];
  }

  /**
   * @dataProvider databasePathProvider
   */
  public function testdatabasePathPrintsAbsolutePathToFileExtractedFromConfiguration(
    $args,
    $result
  ): void {
    $path = fsys\databasePath(...$args);

    $this->assertEquals($path, $result);
  }
}
