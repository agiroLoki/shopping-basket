<?php

declare(strict_types=1);

namespace App\Tests\Router;

use App\Router as r;
use \React\{
  Http\Message\ServerRequest,
  Http\Message\Response
};

class ActionsTest extends \seregazhuk\React\PromiseTesting\TestCase
{
  public function pathProvider(): array
  {
    return [
      ['/api/products', ['api', 'products']],
      ['api/cart', ['api', 'cart']],
      ['foo-bar', ['foo-bar']],
    ];
  }

  /**
   * @dataProvider pathProvider
   */
  public function testsplitPathTokenizesURIPathFragment($path, $res): void
  {
    $tokenized = r\splitPath($path);

    $this->assertEquals($res, $tokenized);
    $this->assertIsArray($tokenized);
  }

  public function routerProvider(): array
  {
    return [
      [
        new ServerRequest('GET', 'http://mwl-project.local/api/products', [
          'Content-Type' => 'application/json',
        ]),
        ['PRODUCTS_DB' => 'products.json'],
        [
          [
            'name'  => 'Pioneer DJ Mixer',
            'price' => 699,
          ],
          [
            'name'  => 'Roland Wave Sampler',
            'price' => 485,
          ],
          [
            'name'  => 'Reloop Headphone',
            'price' => 159,
          ],
          [
            'name'  => 'Rokit Monitor',
            'price' => 189.9,
          ],
          [
            'name'  => 'Fisherprice Baby Mixer',
            'price' => 120,
          ],
        ],
      ],
      [
        (new ServerRequest('GET', 'http://localhost/recipes'))
          ->withParsedBody(['meal' => 'FishStew']),
        [],
        ['error' => 'Resource not found'],
      ],
    ];
  }

  /**
   * @dataProvider routerProvider
   */
  public function testrouterCreatesApplicationRouter($req, $config, $response): void
  {
    $router = r\router($req, $this->eventLoop(), $config);

    $this->assertPromiseFulfillsWithInstanceOf($router, Response::class);
    $this->assertTrueAboutPromise(
      $router,
      fn ($res) =>
        \json_decode($res->getBody()->getContents(), true) === $response,
    );
  }
}
