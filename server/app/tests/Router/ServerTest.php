<?php

declare(strict_types=1);

namespace App\Tests\Router;

use App\Router as r;
use \React\{
  Http\Server,
  EventLoop\Factory,
  EventLoop\LoopInterface,
  Http\Message\Response,
  Socket\Server as Socket,
};
use \Chemem\Bingo\Functional\{
  Functors\Monads\Reader,
};
use function \React\Promise\resolve;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \WyriHaximus\React\Http\Middleware\CustomRequestBodyParsers;

class ServerTest extends \PHPUnit\Framework\TestCase
{
  private LoopInterface $loop;

  public function setUp(): void
  {
    $this->loop = Factory::create();
  }

  public function loadConfigProvider(): array
  {
    return [
      [__DIR__, []],
      [
        __DIR__ . '/../../../',
        [
          'PORT'          => '5000',
          'USERS_DB'      => 'users.json',
          'PRODUCTS_DB'   => 'products.json',
          'DISCOUNTS_DB'  => 'discounts.json',
          'JWT_SECRET'    => '903d3d928e1c92aaafd75d7701a5fe8d',
        ],
      ],
    ];
  }

  /**
   * @dataProvider loadConfigProvider
   */
  public function testloadConfigExtractsContentsofEnvFileIntoArray($path, $result): void
  {
    $config = r\loadConfig($path);

    $this->assertIsArray($config);
    $this->assertEquals($config, $result);
  }

  public function serverProvider(): array
  {
    return [
      [
        [
          fn (Request $req, callable $next) =>
            resolve($next($req))->then(
              fn ($res) => $res->withHeader('Content-Type', 'application/json'),
            ),
          new CustomRequestBodyParsers(),
        ],
        [
          fn (Request $req, callable $next) =>
            resolve($next($req))->then(
              fn ($res) => $res->withHeader('Content-Type', 'text/html'),
            ),
          r\internalErrorMiddleware,
        ],
      ],
    ];
  }
  
  /**
   * @dataProvider serverProvider
   */
  public function testserverCreatesReactHttpServerInstance($middleware): void
  {
    $server = r\server($this->loop, ...$middleware);

    $this->assertInstanceOf(Reader::class, $server);
    $this->assertInstanceOf(Server::class, $server->run([]));
  }

  public function socketServerProvider(): array
  {
    return [
      [
        // server function
        fn (Request $req) =>
          new Response(
            200,
            ['Content-Type' => 'application/json'],
            \json_encode($req->getParsedBody() ?? []),
          ),
      ],
      [
        fn (Request $req) =>
          new Response(
            400,
            ['Content-Type' => 'text/plain'],
            $req->getHeaderLine('Cookie') ?? '',
          ),
      ],
    ];
  }

  /**
   * @dataProvider socketServerProvider
   */
  public function testsocketServerOutputsServerInstance($handler): void
  {
    $socket = r\socketServer(
      new Server($this->loop, $handler),
      $this->loop,
    );

    $this->assertInstanceOf(Reader::class, $socket);
    $this->assertInstanceOf(Server::class, $socket->run([]));
  }

  public function appProvider(): array
  {
    return [
      [
        [
          fn (Request $req, callable $next) =>
            resolve($next($req))->then(
              fn ($res) => $res->withHeader('Content-Type', 'application/json'),
            ),
          new CustomRequestBodyParsers(),
        ],
        [
          'PORT'          => '5000',
          'USERS_DB'      => 'users.json',
          'PRODUCTS_DB'   => 'products.json',
          'DISCOUNTS_DB'  => 'discounts.json',
          'JWT_SECRET'    => '903d3d928e1c92aaafd75d7701a5fe8d',
        ],
      ],
      [[r\internalErrorMiddleware], []],
      [[], []],
    ];
  }

  /**
   * @dataProvider appProvider
   */
  public function testappCreatesRunningServerInstance($middleware, $config): void
  {
    $app = r\app($this->loop, ...$middleware);

    $this->assertInstanceOf(Reader::class, $app);
    $this->assertInstanceOf(Server::class, $app->run($config));
  }
}
