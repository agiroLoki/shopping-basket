<?php

declare(strict_types=1);

namespace App\Tests\Http\Auth;

use App\Http\Auth as a;
use \React\{
  Http\Message\Response,
  Promise\PromiseInterface,
  Http\Message\ServerRequest
};
use function App\Http\httpResponse;
use \Chemem\Bingo\Functional\Algorithms as f;

class ControllersTest extends \seregazhuk\React\PromiseTesting\TestCase
{
  public function loginProvider(): array
  {
    return [
      // invalid method, invalid config
      [
        [
          new ServerRequest('GET', $GLOBALS['base_uri'], ['Content-Type' => 'application/json']),
          [],
        ],
        [
          'body' => ['error' => 'Invalid method'],
          'code' => 405,
        ],
      ],
      // valid method, no body, invalid config
      [
        [
          new ServerRequest('POST', $GLOBALS['base_uri']),
          [],
        ],
        [
          'body' => ['error' => 'Invalid request body'],
          'code' => 400,
        ],
      ],
      // valid method, invalid credentials, invalid config
      [
        [
          (new ServerRequest('POST', $GLOBALS['base_uri']))
            ->withParsedBody([
              'username' => 'ace411',
              'password' => 'bus_stop',
            ]),
          [],
        ],
        [
          'body' => ['error' => 'Invalid request body'],
          'code' => 400,
        ],
      ],
      // valid method, valid credentials, valid config
      [
        [
          (new ServerRequest('POST', $GLOBALS['base_uri']))
            ->withParsedBody([
              'username' => 'sales',
              'password' => 'sales_dept',
            ]),
          [
            'JWT_SECRET'  => '903d3d928e1c92aaafd75d7701a5fe8d',
            'USERS_DB'    => 'users.json',
          ],
        ],
        [
          'body' => [
            'token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJtYWRld2l0aGxvdmUtcHJvamVjdCIsImF1ZCI6Im1hZGV3aXRobG92ZS1wcm9qZWN0IiwidXNlcm5hbWUiOiJzYWxlcyJ9.2-FyOvqP1c9ZpL4cQXvkEsHvtWzRW5Bl6xTC0pu71og',
          ],
          'code' => 200,
        ],
      ],
      // valid method, valid credentials, invalid config
      [
        [
          (new ServerRequest('POST', $GLOBALS['base_uri']))
            ->withParsedBody([
              'username' => 'sales',
              'password' => 'sales_dept',
            ]),
          [],
        ],
        [
          'body' => ['error' => 'Internal Server Error'],
          'code' => 500,
        ],
      ],
    ];
  }

  /**
   * @dataProvider loginProvider
   */
  public function testloginCompletesLoginAction($args, $result): void
  {
    $login = $this->waitForPromise(
      a\login($this->eventLoop(), ...$args)
        ->then(
          null,
          fn ($_) =>
            httpResponse(500, ['error' => 'Internal Server Error']),
        ),
    );

    $response = [
      'body' => \json_decode($login->getBody()->getContents(), true),
      'code' => $login->getStatusCode(),
    ];

    $this->assertInstanceOf(Response::class, $login);
    $this->assertEquals($result, $response);
  }

  public function cartCreateProvider(): array
  {
    return [
      // invalid method, invalid config
      [
        [
          new ServerRequest('POST', $GLOBALS['base_uri']),
          [],
        ],
        405,
      ],
      // valid method, config
      [
        [
          new ServerRequest('GET', $GLOBALS['base_uri']),
          [],
        ],
        201,
      ],
    ];
  }

  /**
   * @dataProvider cartCreateProvider
   */
  public function testcartCreateCreatesJWTWithShoppingCartData($args, $code): void
  {
    $response = $this->waitForPromise(
      a\cartCreate($this->eventLoop(), ...$args)->then(
        null,
        fn ($_) => httpResponse(500, ['error' => 'Internal Server Error']),
      ),
    );

    $getcookie = f\compose(
      f\partialRight(f\pluck, [''], 'Set-Cookie'),
      f\head,
    );

    $body  = \json_decode($response->getBody()->getContents(), true);
    $token = $getcookie($response->getHeaders());

    $this->assertInstanceOf(Response::class, $response);
    $this->assertEquals($code, $response->getStatusCode());
    $this->assertTrue(isset($body['error']) || isset($body['ok']));
    $this->assertTrue(empty($token) || strlen($token) > 0);
  }

  public function getFlaggedProductsProvider(): array
  {
    return [
      // invalid method, invalid config
      [
        [
          new ServerRequest('POST', $GLOBALS['base_uri']),
          [],
        ],
        405,
      ],
      // valid method, invalid auth header
      [
        [
          new ServerRequest('GET', $GLOBALS['base_uri'], [
            'X-Authorization' => 'foo-bar',
          ]),
          [],
        ],
        401,
      ],
      // valid method, valid auth header, invalid token, invalid config
      [
        [
          new ServerRequest('GET', $GLOBALS['base_uri'], [
            'Authorization' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJtYWRld2l0aGxvdmUtcHJvamVjdCIsImF1ZCI6Im1hZGV3aXRobG92ZS1wcm9qZWN0IiwidXNlciI6Im1haWxlciJ9.UrzdV0H-jpgOMzmwDJKzw1LYF3JiSTShQ67XXRRnHbA',
          ]),
          [],
        ],
        401,
      ],
      // valid method, valid auth header, valid token, valid config
      [
        [
          new ServerRequest('GET', $GLOBALS['base_uri'], [
            'Authorization' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJtYWRld2l0aGxvdmUtcHJvamVjdCIsImF1ZCI6Im1hZGV3aXRobG92ZS1wcm9qZWN0IiwidXNlcm5hbWUiOiJzYWxlcyJ9.2-FyOvqP1c9ZpL4cQXvkEsHvtWzRW5Bl6xTC0pu71og',
          ]),
          [
            'DISCOUNTS_DB' => 'discounts.json',
            'JWT_SECRET'   => '903d3d928e1c92aaafd75d7701a5fe8d',
          ],
        ],
        200,
      ],
    ];
  }

  /**
   * @dataProvider getFlaggedProductsProvider
   */
  public function testgetFlaggedProductsAccessesDataInStoreContainingFlaggedProducts(
    $args,
    $code
  ): void {
    $flagged = a\getFlaggedProducts($this->eventLoop(), ...$args);
    
    $this->assertTrueAboutPromise(
      $flagged,
      fn ($res) => $res instanceof Response,
    );

    $this->assertTrueAboutPromise(
      $flagged,
      fn ($res) => $res->getStatusCode() === $code,
    );
    
    $this->assertTrueAboutPromise(
      $flagged,
      function ($res) {
        $body = \json_decode($res->getBody()->getContents(), true);

        return \is_array($body) || isset($body['error']);
      },
    );
  }
}
