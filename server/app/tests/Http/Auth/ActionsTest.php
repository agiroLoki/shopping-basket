<?php

declare(strict_types=1);

namespace App\Tests\Http\Auth;

use App\{
  Http,
  Http\Auth as a,
};
use \Chemem\Bingo\Functional\Algorithms as f;

class ActionsTest extends \PHPUnit\Framework\TestCase
{
  public function jwtEncodeProvider(): array
  {
    return [
      [
        [
          '@secret',
          [
            'handle'  => '@ace411',
            'project' => 'mwl-cart',
          ],
        ],
        'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJtYWRld2l0aGxvdmUtcHJvamVjdCIsImF1ZCI6Im1hZGV3aXRobG92ZS1wcm9qZWN0IiwiaGFuZGxlIjoiQGFjZTQxMSIsInByb2plY3QiOiJtd2wtY2FydCJ9.gQAGtmY3JAlG2L6a4NUw8-QrcCTogeRLitb_RSOOzs0',
      ],
      [
        ['', []],
        'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJtYWRld2l0aGxvdmUtcHJvamVjdCIsImF1ZCI6Im1hZGV3aXRobG92ZS1wcm9qZWN0In0.2osPjPzyx19RPThYQa63a4DnV644kNRgO8_SqlNJwp0',
      ],
    ];
  }

  /**
   * @dataProvider jwtEncodeProvider
   */
  public function testjwtEncodeCreatesJWTFromArbitraryClaims($args, $token): void
  {
    $jwt = a\jwtEncode(...$args);

    $this->assertEquals($jwt, $token);
    $this->assertIsString($jwt);
  }

  public function jwtDecodeProvider(): array
  {
    return [
      [
        // valid token, invalid secret
        [
          '@secret',
          'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJtYWRld2l0aGxvdmUtcHJvamVjdCIsImF1ZCI6Im1hZGV3aXRobG92ZS1wcm9qZWN0In0.2osPjPzyx19RPThYQa63a4DnV644kNRgO8_SqlNJwp0',
        ],
        [],
      ],
      // valid token, valid secret
      [
        [
          '@secret',
          'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJtYWRld2l0aGxvdmUtcHJvamVjdCIsImF1ZCI6Im1hZGV3aXRobG92ZS1wcm9qZWN0IiwiaGFuZGxlIjoiQGFjZTQxMSIsInByb2plY3QiOiJtd2wtY2FydCJ9.gQAGtmY3JAlG2L6a4NUw8-QrcCTogeRLitb_RSOOzs0',
        ],
        f\extend(Http\JWT_BASE_CLAIMS, [
          'handle'  => '@ace411',
          'project' => 'mwl-cart',
        ]),
      ],
      // invalid token, secret
      [
        ['yellow', 'foo.bar193.baz22baz4'],
        [],
      ],
    ];
  }

  /**
   * @dataProvider jwtDecodeProvider
   */
  public function testjwtDecodeDecodesJWTIntoArrayOfClaims($args, $result): void
  {
    $claims = a\jwtDecode(...$args);

    $this->assertEquals($result, $claims);
  }

  public function filterCredentialsProvider(): array
  {
    // default database for comparisons
    $defaultdb = [
      [
        'username' => 'ace411',
        'password' => '$argon2i$v=19$m=65536,t=4,p=1$xuj+7nlzIRWgu1J0UMx7Vw$eNNMoGK9gHp4ctg2WiOqe9SrXdSffzDFlbe5lv0YrbA',
      ],
    ];

    return [
      // invalid user, pass
      [
        [
          [
            'username' => 'sales',
            'password' => 'sales_dept',
          ],
          $defaultdb,
        ],
        [],
      ],
      // valid user, invalid pass
      [
        [
          [
            'username' => 'ace411',
            'password' => 'sales_dept',
          ],
          $defaultdb,
        ],
        [],
      ],
      // valid user, valid pass
      [
        [
          [
            'username' => 'ace411',
            'password' => 'crooked_pwd',
          ],
          $defaultdb,
        ],
        [
          'username' => 'ace411',
          'password' => 'crooked_pwd',
        ],
      ],
    ];
  }

  /**
   * @dataProvider filterCredentialsProvider
   */
  public function testfilterCredentialsValidatesUserCredentials($args, $result): void
  {
    $credentials = a\filterCredentials(...$args);

    $this->assertEquals($result, $credentials);
    $this->assertIsArray($credentials);
  }

  public function filterAuthTokenProvider(): array
  {
    return [
      // invalid token, secret
      [
        ['', '@secret'],
        [],
      ],
      // valid token, invalid secret
      [
        [
          'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJtYWRld2l0aGxvdmUtcHJvamVjdCIsImF1ZCI6Im1hZGV3aXRobG92ZS1wcm9qZWN0IiwidXNlciI6Im1pa2UifQ.zW5Bz9yG6qCGHdY0VxFJ3XXet7Wm8U2C8cTdP73-GoA',
          'a-secret',
        ],
        [],
      ],
      // invalid token, valid secret
      [
        [
          'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJtYWRld2l0aGxvdmUtcHJvamVjdCIsImF1ZCI6Im1hZGV3aXRobG92ZS1wcm9qZWN0IiwidXNlciI6Im1pa2UifQ.zW5Bz9yG6qCGHdY0VxFJ3XXet7Wm8U2C8cTdP73-GoA',
          '@secret',
        ],
        [],
      ],
      // valid token, valid secret
      [
        [
          'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJtYWRld2l0aGxvdmUtcHJvamVjdCIsImF1ZCI6Im1hZGV3aXRobG92ZS1wcm9qZWN0IiwidXNlcm5hbWUiOiJtaWtlIn0.qH-6iq5f-wjIpaLQrSngDquZ7ovyAMyO6FYlsEKopVI',
          '@secret',
        ],
        f\extend(Http\JWT_BASE_CLAIMS, [
          'username' => 'mike',
        ]),
      ],
    ];
  }

  /**
   * @dataProvider filterAuthTokenProvider
   */
  public function testfilterAuthTokenValidatesTokenForLogins($args, $result): void
  {
    $claims = a\filterAuthToken(...$args);

    $this->assertEquals($result, $claims);
    $this->assertIsArray($claims);
  }
}
