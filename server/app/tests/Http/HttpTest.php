<?php

declare(strict_types=1);

namespace App\Tests\Http;

use function App\Http\httpResponse;
use \React\{
  Http\Message\Response,
  Promise\PromiseInterface,
};

class HttpTest extends \seregazhuk\React\PromiseTesting\TestCase
{
  public function httpResponseProvider(): array
  {
    return [
      [
        [200, ['ok' => true], ['Content-Type' => 'application/json']],
        ['ok' => true],
      ],
      [
        [400, ['error' => 'Bad request']],
        ['error' => 'Bad request'],
      ],
    ];
  }

  /**
   * @dataProvider httpResponseProvider
   */
  public function testhttpResponseProviderSubsumesHttpResponseInPromise($args, $response): void
  {
    $promise = httpResponse(...$args);

    $this->assertTrue($promise instanceof PromiseInterface);
    $this->assertTrueAboutPromise(
      $promise,
      fn ($res) =>
        \json_decode($res->getBody()->getContents(), true) === $response,
    );
    $this->assertTrueAboutPromise(
      $promise,
      fn ($res) => $res instanceof Response,
    );
  }
}
