<?php

declare(strict_types=1);

namespace App\Tests\Http\Products;

use App\{
  Http\Products as p,
  Filesystem as fsys,
};
use \React\{
  Http\Message\Response,
  Promise\PromiseInterface,
  Http\Message\ServerRequest
};
use \Chemem\Bingo\Functional\Algorithms as f;

class ControllersTest extends \seregazhuk\React\PromiseTesting\TestCase
{
  public function getProductsProvider(): array
  {
    return [
      // invalid method, invalid config
      [
        [
          new ServerRequest('PUT', $GLOBALS['base_uri']),
          [],
        ],
        [
          'body' => ['error' => 'Invalid method'],
          'code' => 405,
        ],
      ],
      // valid method, config
      [
        [
          new ServerRequest('GET', $GLOBALS['base_uri']),
          [],
        ],
        [
          'body' => [
            [
              'name'  => 'Pioneer DJ Mixer',
              'price' => 699,
            ],
            [
              'name'  => 'Roland Wave Sampler',
              'price' => 485,
            ],
            [
              'name'  => 'Reloop Headphone',
              'price' => 159,
            ],
            [
              'name'  => 'Rokit Monitor',
              'price' => 189.9,
            ],
            [
              'name'  => 'Fisherprice Baby Mixer',
              'price' => 120,
            ],
          ],
          'code' => 200,
        ],
      ],
    ];
  }

  /**
   * @dataProvider getProductsProvider
   */
  public function testgetProductsReturnsListOfAvailableProducts($args, $result): void
  {
    $products = $this->waitForPromise(
      p\getProducts($this->eventLoop(), ...$args),
    );

    $response = [
      'code' => $products->getStatusCode(),
      'body' => \json_decode($products->getBody()->getContents(), true),
    ];

    $this->assertInstanceOf(Response::class, $products);
    $this->assertEquals($result, $response);
  }

  public function addToCartProvider(): array
  {
    return [
      // valid method, valid cart, valid config, invalid body
      [
        [
          (new ServerRequest('POST', $GLOBALS['base_uri'], [
            'Cookie' => f\concat(
              '=',
              'cart',
              'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJtYWRld2l0aGxvdmUtcHJvamVjdCIsImF1ZCI6Im1hZGV3aXRobG92ZS1wcm9qZWN0IiwiY2FydGlkIjoiNDgxYzE1MDFkMiIsImNhcnQiOlt7Im5hbWUiOiJSb2tpdCBNb25pdG9yIiwicXVhbnRpdHkiOjN9XX0.RY1OxQKcyiDl7FeLE_5pxcF3taw1wphtPwsvauqjtVk',
            ),
          ]))->withParsedBody([
            'name'     => 'Cyberpunk 2077',
            'quantity' => 3,
          ]),
          [
            'JWT_SECRET'  => '903d3d928e1c92aaafd75d7701a5fe8d',
            'PRODUCTS_DB' => 'products.json',
          ],
        ],
        [
          'body'   => ['error' => 'Invalid request data'],
          'code'   => 400,
          'cookie' => '',
        ],
      ],
      // valid method, valid cart, valid config, valid body
      [
        [
          (new ServerRequest('POST', $GLOBALS['base_uri'], [
            'Cookie' => f\concat(
              '=',
              'cart',
              'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJtYWRld2l0aGxvdmUtcHJvamVjdCIsImF1ZCI6Im1hZGV3aXRobG92ZS1wcm9qZWN0IiwiY2FydGlkIjoiNDgxYzE1MDFkMiIsImNhcnQiOltdfQ.WNdmPZ3R6ou0KL2exQkuIW9Ly1NeO4VLg4lD_29D7kM',
            ),
          ]))->withParsedBody([
            'name'     => 'Rokit Monitor',
            'quantity' => 3,
          ]),
          [
            'JWT_SECRET'  => '903d3d928e1c92aaafd75d7701a5fe8d',
            'PRODUCTS_DB' => 'products.json',
          ],
        ],
        [
          'body'   => [
            'cart' => [
              [
                'name'     => 'Rokit Monitor',
                'quantity' => 3,
                'cost'     => 569.7,
              ],
            ],
          ],
          'code'   => 200,
          'cookie' => 'cart=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJtYWRld2l0aGxvdmUtcHJvamVjdCIsImF1ZCI6Im1hZGV3aXRobG92ZS1wcm9qZWN0IiwiY2FydGlkIjoiNDgxYzE1MDFkMiIsImNhcnQiOlt7Im5hbWUiOiJSb2tpdCBNb25pdG9yIiwicXVhbnRpdHkiOjMsImNvc3QiOjU2OS43fV19.ylQC1c3mKZUUWl3cp1-5FW_1n0N5LEJLZkLZ3w544jQ',
        ],
      ],
    ];
  }

  /**
   * @dataProvider addToCartProvider
   */
  public function testaddToCartAddsItemsToACart($args, $result): void
  {
    $add = $this->waitForPromise(
      p\addToCart($this->eventLoop(), ...$args),
    );
    $getcookie = f\compose(
      f\partialRight(f\pluck, [''], 'Set-Cookie'),
      f\head,
    );

    $response = [
      'body'   => \json_decode($add->getBody()->getContents(), true),
      'code'   => $add->getStatusCode(),
      'cookie' => $getcookie($add->getHeaders()),
    ];

    $this->assertEquals($result, $response);
  }

  public function removeFromCartProvider(): array
  {
    $config = [
      'JWT_SECRET'   => '903d3d928e1c92aaafd75d7701a5fe8d',
      'PRODUCTS_DB'  => 'products.json',
      // dump contents to test db
      'DISCOUNTS_DB' => 'tests.json',
    ];

    $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJtYWRld2l0aGxvdmUtcHJvamVjdCIsImF1ZCI6Im1hZGV3aXRobG92ZS1wcm9qZWN0IiwiY2FydGlkIjoiNDgxYzE1MDFkMiIsImNhcnQiOlt7Im5hbWUiOiJSZWxvb3AgSGVhZHBob25lIiwicXVhbnRpdHkiOjEwLCJjb3N0IjoxNTkwfSx7Im5hbWUiOiJSb2tpdCBNb25pdG9yIiwicXVhbnRpdHkiOjMsImNvc3QiOjU2OS43fV19.9fsSNXTzXU0b5VKVbke0j6D9STCrFy_u_xFiglwevdI';

    return [
      // valid method, valid cart, valid config, invalid body
      [
        [
          (new ServerRequest('POST', $GLOBALS['base_uri'], [
            'Cookie' => f\concat('=', 'cart', $token),
          ]))->withParsedBody([
            'name'     => 'God of War: Ragnarok',
            'quantity' => 2,
          ]),
          $config,
        ],
        [
          'code'   => 400,
          'body'   => ['error' => 'Invalid request data'],
          'cookie' => '',
        ],
      ],
      // valid method, valid cart, valid config, invalid body
      [
        [
          (new ServerRequest('POST', $GLOBALS['base_uri'], [
            'Cookie' => f\concat('=', 'cart', $token),
          ]))->withParsedBody([
            'name'     => 'Roland Wave Sampler',
            'quantity' => 2,
          ]),
          $config,
        ],
        [
          'code'   => 400,
          'body'   => ['error' => 'Item is not in cart'],
          'cookie' => '',
        ],
      ],
      // valid method, valid cart, valid config, valid body
      [
        [
          (new ServerRequest('POST', $GLOBALS['base_uri'], [
            'Cookie' => f\concat('=', 'cart', $token),
          ]))->withParsedBody([
            'name'     => 'Reloop Headphone',
            'quantity' => 10,
          ]),
          $config,
          ],
          [
            'code' => 200,
            'body' => [
              [
                'cart' => [
                  'name'     => 'Rokit Monitor',
                  'quantity' => 3,
                  'cost'     => 569.7,
                ],
              ],
            ],
            'cookie' => 'cart=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJtYWRld2l0aGxvdmUtcHJvamVjdCIsImF1ZCI6Im1hZGV3aXRobG92ZS1wcm9qZWN0IiwiY2FydGlkIjoiNDgxYzE1MDFkMiIsImNhcnQiOlt7Im5hbWUiOiJSb2tpdCBNb25pdG9yIiwicXVhbnRpdHkiOjMsImNvc3QiOjU2OS43fV19.ylQC1c3mKZUUWl3cp1-5FW_1n0N5LEJLZkLZ3w544jQ',
          ],
      ],
    ];
  }

  /**
   * @dataProvider removeFromCartProvider
   */
  public function removeFromCartRemovesItemsFromACart($args, $result): void
  {
    $remove = $this->waitForPromise(
      p\removeFromCart($this->eventLoop(), ...$args),
    );

    $getcookie = f\compose(
      f\partialRight(f\pluck, [''], 'Set-Cookie'),
      f\head,
    );

    $response = [
      'code'   => $remove->getStatusCode(),
      'body'   => \json_decode($remove->getBody()->getContents(), true),
      'cookie' => $getcookie($remove->getHeaders()),
    ];

    $this->assertEquals($result, $response);
    $this->assertTrueAboutPromise(
      fsys\fileRead(
        $this->eventLoop(),
        f\filePath(0, 'storage', 'tests.json'),
      ),
      fn ($contents) => \is_string($contents) && \strlen($contents) > 0,
    );
  }

  public function checkoutProvider(): array
  {
    return [
      // invalid method
      [
        [
          new ServerRequest('POST', $GLOBALS['base_uri']),
          [],
        ],
        [
          'body'    => ['error' => 'Invalid method'],
          'code'    => 405,
          'cookie'  => '',
        ],
      ],
      // valid method, invalid cart, invalid secret
      [
        [
          new ServerRequest('GET', $GLOBALS['base_uri'], [
            'Cookie' => 'cart=abc123.def456',
          ]),
          [
            'JWT_SECRET' => '@secret',
          ],
        ],
        [
          'body'    => ['error' => 'Cart is invalid'],
          'code'    => 400,
          'cookie'  => '',
        ],
      ],
      // valid method, invalid cart, valid config
      [
        [
          new ServerRequest('GET', $GLOBALS['base_uri'], [
            'Cookie' => 'cart=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJtYWRld2l0aGxvdmUtcHJvamVjdCIsImF1ZCI6Im1hZGV3aXRobG92ZS1wcm9qZWN0IiwiaGFuZGxlIjoiQGFjZTQxMSIsInByb2plY3QiOiJtd2wtY2FydCJ9.gQAGtmY3JAlG2L6a4NUw8-QrcCTogeRLitb_RSOOzs0',
          ]),
          [
            'JWT_SECRET' => '903d3d928e1c92aaafd75d7701a5fe8d',
          ],
        ],
        [
          'body'    => ['error' => 'Cart is invalid'],
          'code'    => 400,
          'cookie'  => '',
        ],
      ],
      // valid method, valid cart, valid config
      [
        [
          new ServerRequest('GET', $GLOBALS['base_uri'], [
            'Cookie' => 'cart=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJtYWRld2l0aGxvdmUtcHJvamVjdCIsImF1ZCI6Im1hZGV3aXRobG92ZS1wcm9qZWN0IiwiY2FydGlkIjoiNDgxYzE1MDFkMiIsImNhcnQiOlt7Im5hbWUiOiJSb2tpdCBNb25pdG9yIiwicXVhbnRpdHkiOjMsImNvc3QiOjU2OS43fV19.ylQC1c3mKZUUWl3cp1-5FW_1n0N5LEJLZkLZ3w544jQ',
          ]),
          [
            'JWT_SECRET' => '903d3d928e1c92aaafd75d7701a5fe8d',
          ],
        ],
        [
          'body'   => ['ok' => true],
          'code'   => 200,
          'cookie' => 'cart=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJtYWRld2l0aGxvdmUtcHJvamVjdCIsImF1ZCI6Im1hZGV3aXRobG92ZS1wcm9qZWN0IiwiY2FydGlkIjoiNDgxYzE1MDFkMiIsImNhcnQiOltdfQ.WNdmPZ3R6ou0KL2exQkuIW9Ly1NeO4VLg4lD_29D7kM',
        ],
      ],
    ];
  }

  /**
   * @dataProvider checkoutProvider
   */
  public function testcheckoutCompletesCheckoutAction($args, $result): void
  {
    $checkout = $this->waitForPromise(
      p\checkout($this->eventLoop(), ...$args),
    );

    $getcookie = f\compose(
      f\partialRight(f\pluck, [''], 'Set-Cookie'),
      f\head,
    );

    $response = [
      'code'   => $checkout->getStatusCode(),
      'body'   => \json_decode($checkout->getBody()->getContents(), true),
      'cookie' => $getcookie($checkout->getHeaders()),
    ];

    $this->assertEquals($result, $response);
  }
}
