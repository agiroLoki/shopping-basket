<?php

declare(strict_types=1);

namespace App\Tests\Http\Products;

use App\{Http, Http\Products as p};
use \React\{
  Promise\PromiseInterface,
  Http\Message\ServerRequest
};
use \Chemem\Bingo\Functional\Algorithms as f;
use function App\Http\httpResponse;

class ActionsTest extends \seregazhuk\React\PromiseTesting\TestCase
{
  public function filterCheckoutDataProvider(): array
  {
    $products = [
      [
        'name'  => 'Pioneer DJ Mixer',
        'price' => 699,
      ],
      [
        'name'  => 'Roland Wave Sampler',
        'price' => 485,
      ],
    ];

    return [
      // invalid checkout
      [
        [['name' => 'Chicken Pizza', 'quantity' => 2], $products],
        [],
      ],
      // invalid fields
      [
        [['name' => 'Fish Fillet', 'count' => 15], $products],
        [],
      ],
      // invalid quantity
      [
        [['name' => 'Chicken Tikka', 'quantity' => 'three'], $products],
        [],
      ],
      // invalid field
      [
        [['name' => 'Pioneer DJ Mixer', 'quantity' => 0], $products],
        [],
      ],
      // valid data
      [
        [['name' => 'Roland Wave Sampler', 'quantity' => 12], $products],
        [
          'name'     => 'Roland Wave Sampler',
          'quantity' => 12,
          'cost'     => 5820,
        ],
      ],
    ];
  }

  /**
   * @dataProvider filterCheckoutDataProvider
   */
  public function testfilterCheckoutDataValidatesCheckoutData($args, $res): void
  {
    $filter = p\filterCheckoutData(...$args);

    $this->assertEquals($res, $filter);
    $this->assertIsArray($filter);
  }

  public function filterCartTokenProvider(): array
  {
    return [
      // valid token, invalid secret
      [
        [
          'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJtYWRld2l0aGxvdmUtcHJvamVjdCIsImF1ZCI6Im1hZGV3aXRobG92ZS1wcm9qZWN0IiwiY2FydGlkIjoiOTAxNWI4NGNkNSIsImNhcnQiOltdfQ.RxObjmei9iBKbxuFWIIy5o-UbA0C23_Rd7KRwPYNFZE',
          '@another_secret',
        ],
        [],
      ],
      // valid token, valid secret
      [
        [
          'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJtYWRld2l0aGxvdmUtcHJvamVjdCIsImF1ZCI6Im1hZGV3aXRobG92ZS1wcm9qZWN0IiwiY2FydGlkIjoiOTAxNWI4NGNkNSIsImNhcnQiOltdfQ.RxObjmei9iBKbxuFWIIy5o-UbA0C23_Rd7KRwPYNFZE',
          '@secret',
        ],
        f\extend(Http\JWT_BASE_CLAIMS, [
          'cart'   => [],
          'cartid' => "9015b84cd5",
        ]),
      ],
      // invalid token, secret
      [
        ['abc123.def456', 'yellow'],
        [],
      ],
    ];
  }

  /**
   * @dataProvider filterCartTokenProvider
   */
  public function testfilterCartTokenValidatesCartJWT($args, $res): void
  {
    $filter = p\filterCartToken(...$args);

    $this->assertEquals($res, $filter);
    $this->assertIsArray($filter);
  }

  public function popFromCartPayloadProvider(): array
  {
    return [
      [
        [
          [
            'cart' => [
              ['name' => 'Chicken Fillet', 'quantity' => 3],
              ['name' => 'Beef Lovers Pizza', 'quantity' => 1],
            ],
          ],
          ['name' => 'Fish Fillet', 'quantity' => 2],
        ],
        [
          'cart' => [
            ['name' => 'Chicken Fillet', 'quantity' => 3],
            ['name' => 'Beef Lovers Pizza', 'quantity' => 1],
          ],
        ],
      ],
      [
        [
          [
            'cart' => [
              ['name' => 'Chicken Fillet', 'quantity' => 3],
              ['name' => 'Fish Fillet', 'quantity' => 2],
            ],
          ],
          ['name' => 'Fish Fillet', 'quantity' => 2],
        ],
        [
          'cart' => [
            ['name' => 'Chicken Fillet', 'quantity' => 3],
          ],
        ],
      ],
      [
        [
          [
            'cart' => [
              ['name' => 'Chicken Fillet', 'quantity' => 3],
              ['name' => 'Fish Fillet', 'quantity' => 2],
            ],
          ],
          ['name' => 'Fish Fillet', 'quantity' => 1],
        ],
        [
          'cart' => [
            ['name' => 'Chicken Fillet', 'quantity' => 3],
            ['name' => 'Fish Fillet', 'quantity' => 2],
          ],
        ],
      ],
    ];
  }

  /**
   * @dataProvider popFromCartPayloadProvider
   */
  public function testpopFromCartRemovesCheckoutDataFromPayload($args, $result): void
  {
    $cart = p\popFromCartPayload(...$args);

    $this->assertEquals($result, $cart);
  }

  public function pushToCartPayloadProvider(): array
  {
    return [
      [
        [
          ['cart' => []],
          ['name' => 'CyberPunk 2077', 'quantity' => 2],
        ],
        [
          'cart' => [
            ['name' => 'CyberPunk 2077', 'quantity' => 2],
          ],
        ],
      ],
      [
        [
          [
            'cart' => [
              ['name' => 'Ghost of Tsushima', 'quantity' => 8],
              ['name' => 'God of War', 'quantity' => 9],
            ],
          ],
          ['name' => 'NFS Heat', 'quantity' => 1],
        ],
        [
          'cart' => [
            ['name' => 'NFS Heat', 'quantity' => 1],
            ['name' => 'Ghost of Tsushima', 'quantity' => 8],
            ['name' => 'God of War', 'quantity' => 9],
          ],
        ],
      ],
    ];
  }

  /**
   * @dataProvider pushToCartPayloadProvider
   */
  public function testpushToCartPayloadAddsCheckoutDataToPayload($args, $result): void
  {
    $cart = p\pushToCartPayload(...$args);

    $this->assertEquals($result, $cart);
  }

  public function modifyCartProvider(): array
  {
    $handler = fn (array $claims, array $checkout) =>
      httpResponse(200, [
        'cart'      => f\pluck($claims, 'cart'),
        'checkout'  => $checkout,
      ]);

    return [
      // invalid method, invalid config
      [
        [
          new ServerRequest('GET', $GLOBALS['base_uri']),
          [],
          $handler,
        ],
        [
          'code' => 405,
          'body' => ['error' => 'Invalid method'],
        ],
      ],
      // valid method, invalid cart, invalid config
      [
        [
          new ServerRequest('POST', $GLOBALS['base_uri'], [
            'Cookie' => 'cart=abc123.def456',
          ]),
          [],
          $handler,
        ],
        [
          'code' => 400,
          'body' => ['error' => 'Cart is invalid'],
        ],
      ],
      // valid method, non-existent cart, invalid config
      [
        [
          new ServerRequest('POST', $GLOBALS['base_uri'], [
            'Cookie' => 'basket=foo',
          ]),
          [],
          $handler,
        ],
        [
          'code' => 400,
          'body' => ['error' => 'Cart is invalid'],
        ],
      ],
      // valid method, valid cart, invalid config
      [
        [
          new ServerRequest('POST', $GLOBALS['base_uri'], [
            'Cookie' => f\concat(
              '=',
              'cart',
              'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJtYWRld2l0aGxvdmUtcHJvamVjdCIsImF1ZCI6Im1hZGV3aXRobG92ZS1wcm9qZWN0IiwiY2FydGlkIjoiNDgxYzE1MDFkMiIsImNhcnQiOlt7Im5hbWUiOiJSb2tpdCBNb25pdG9yIiwicXVhbnRpdHkiOjN9XX0.RY1OxQKcyiDl7FeLE_5pxcF3taw1wphtPwsvauqjtVk',
            ),
          ]),
          [],
          $handler,
        ],
        [
          'code' => 400,
          'body' => ['error' => 'Cart is invalid'],
        ],
      ],
      // valid method, valid cart, valid config
      [
        [
          new ServerRequest('POST', $GLOBALS['base_uri'], [
            'Cookie' => f\concat(
              '=',
              'cart',
              'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJtYWRld2l0aGxvdmUtcHJvamVjdCIsImF1ZCI6Im1hZGV3aXRobG92ZS1wcm9qZWN0IiwiY2FydGlkIjoiNDgxYzE1MDFkMiIsImNhcnQiOlt7Im5hbWUiOiJSb2tpdCBNb25pdG9yIiwicXVhbnRpdHkiOjMsImNvc3QiOjU2OS43fV19.ylQC1c3mKZUUWl3cp1-5FW_1n0N5LEJLZkLZ3w544jQ',
            ),
          ]),
          [
            'JWT_SECRET'  => '903d3d928e1c92aaafd75d7701a5fe8d',
            'PRODUCTS_DB' => 'products.json',
          ],
          $handler,
        ],
        [
          'code' => 200,
          'body' => [
            'cart' => [
              [
                'name'     => 'Rokit Monitor',
                'quantity' => 3,
                'cost'     => 569.7,
              ],
            ],
            'checkout' => [],
          ],
        ],
      ],
    ];
  }

  /**
   * @dataProvider modifyCartProvider
   */
  public function testmodifyCartPrimesCartForModification($args, $result): void
  {
    $modify = $this->waitForPromise(
      p\modifyCart($this->eventLoop(), ...$args),
    );

    $response = [
      'code' => $modify->getStatusCode(),
      'body' => \json_decode($modify->getBody()->getContents(), true),
    ];

    $this->assertEquals($result, $response);
  }
}
