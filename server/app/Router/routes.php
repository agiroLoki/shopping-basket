<?php

/**
 * Router/routes.php
 * 
 * Contains routing table and extensible base controller
 * 
 * @author Lochemem Bruno Michael <lochbm@gmail.com>
 */

declare(strict_types=1);

namespace App\Router;

use \React\{
  EventLoop\LoopInterface,
  Promise\PromiseInterface,
  Promise\Promise,
};
use function App\{
  Http\httpResponse,
  Http\Products\getProducts,
  Http\Auth\getFlaggedProducts,
  Http\Auth\login,
  Http\Auth\cartCreate
};
use App\{
  Http\Auth as a,
  Http\Products as p,
};
use \Chemem\Bingo\Functional\{
  Algorithms as f,
  PatternMatching as pm,
};
use \Psr\Http\Message\ServerRequestInterface as Request;

/**
 * defineRoutes
 * creates routing table for app
 * 
 * defineRoutes :: Array -> Object -> Object -> Array -> Promise s a
 * 
 * @param array $route
 * @param Request $request
 * @param LoopInterface $loop
 * @param array $config
 * @return PromiseInterface
 * @example
 * 
 * defineRoutes(['api', 'products'], $request, $loop, [])
 * //=> object(React\Promise\Promise) {}
 */
function defineRoutes(
  array $route,
  Request $request,
  LoopInterface $loop,
  array $config = []
): PromiseInterface {
  // create a decomposed controller placeholder
  $controller = f\partial(baseController, $loop, $request, $config);
  
  return pm\patternMatch([
    // GET /api/products
    '["api", "products"]'       => fn () => $controller(p\getProducts),
    // POST /api/login
    '["api", "login"]'          => fn () => $controller(a\login),
    // GET /api/flagged
    '["api", "flagged"]'        => fn () => $controller(a\getFlaggedProducts),
    // GET /api/cart/create
    '["api", "cart", "create"]' => fn () => $controller(a\cartCreate),
    // POST /api/cart/add
    '["api", "cart", "add"]'    => fn () => $controller(p\addToCart),
    // POST /api/cart/remove
    '["api", "cart", "remove"]' => fn () => $controller(p\removeFromCart),
    // GET /api/checkout
    '["api", "checkout"]'       => fn () => $controller(p\checkout),
    // {ANY} /{any}
    '_'                         => fn () =>
      httpResponse(404, ['error' => 'Resource not found']),
  ], $route);
}

const defineRoutes = __NAMESPACE__ . '\\defineRoutes';

/**
 * baseController
 * applies any universal controller property checks and invokes apt controller
 * 
 * baseController :: Object -> Object -> Array -> (Object -> Object -> Array -> Promise s a) -> Promise s a
 *
 * @param LoopInterface $loop
 * @param Request $request
 * @param array $config
 * @param callable $controller
 * @return PromiseInterface
 */
function baseController(
  LoopInterface $loop,
  Request $request,
  array $config,
  callable $controller
): PromiseInterface {
  return $controller($loop, $request, $config)
    ->then(null, fn ($_) => httpResponse(500, ['Internal Server Error']));
}

const baseController = __NAMESPACE__ . '\\baseController';
