<?php

/**
 * Router/server.php
 * 
 * Contains server-dependent primitives for server process operationalization
 * 
 * @author Lochemem Bruno Michael <lochbm@gmail.com>
 */

declare(strict_types=1);

namespace App\Router;

use App\Http;
use Chemem\Bingo\Functional\{
  Algorithms as f,
  Functors\Monads\Reader,
};
use \React\{
  Http\Server,
  Http\Response,
  Promise\Promise,
  Promise\PromiseInterface,
  EventLoop\LoopInterface,
  Socket\Server as Socket,
  Stream\WritableResourceStream,
};
use \Dotenv\Dotenv;
use \Psr\Http\Message\ServerRequestInterface as Request;

/**
 * app
 * provisions a ReactPHP server complete with all apt middleware and configuration
 *
 * app :: Object -> [Object] -> Reader Object
 * 
 * @param LoopInterface $loop
 * @param object ...$middleware
 * @return Reader
 */
function app(LoopInterface $loop, ...$middleware): Reader
{
  return Reader\withReader(
    // provision the socket server
    f\partialRight(socketServer, $loop),
    // provision the React HTTP server with middleware
    server($loop, ...$middleware),
  );
}

const app = __NAMESPACE__ . '\\app';

/**
 * internalErrorMiddleware
 * ensures that internal server errors - of the 500 ilk - are converted to Promises
 *
 * internalErrorMiddleware :: Object -> (a -> Object) -> Promise s a
 * 
 * @param Request $request
 * @param callable $next
 * @return PromiseInterface
 */
function internalErrorMiddleware(Request $request, callable $next): PromiseInterface
{
  // attempt to resolve all requests - as usual
  $promise = new Promise(fn (callable $res) => $res($next($request)));

  return $promise->then(
    f\identity,
    // convert any erroneous, unresolved server errors to response objects
    fn ($_) =>
      new Response(500, Http\JSON_TYPE, \json_encode([
        'error' => 'Internal Server Error',
      ], JSON_PRETTY_PRINT))
  );
}

const internalErrorMiddleware = __NAMESPACE__ . '\\internalErrorMiddleware';

/**
 * server
 * creates HTTP server instance
 *
 * @param LoopInterface $loop
 * @param object|callable ...$middleware
 * @return Reader
 */
function server(LoopInterface $loop, ...$middleware): Reader
{
  return Reader\reader(
    fn (array $config) =>
      new Server(
        $loop,
        // apply middleware and handler
        ...[
          ...$middleware,
          fn ($request) => router($request, $loop, $config),
        ],
      ),
  );
}

/**
 * socketServer
 * creates a socket
 * 
 * socketServer :: Object -> Object -> Reader Object
 *
 * @param Server $server
 * @param LoopInterface $loop
 * @return Reader
 */
function socketServer(Server $server, LoopInterface $loop): Reader
{
  return Reader\reader(
    function (array $config) use ($loop, $server) {
      // extract the socket port from the configuration
      $port     = f\pluck($config, 'port', 5000);
      $socket   = new Socket($port, $loop);
      // create writable stream instance with special write access to STDOUT
      $writable = new WritableResourceStream(STDOUT, $loop);
      
      $server->listen($socket);
      // print socket address
      $writable->write(
        f\concat(
          ' ',
          'Listening on',
          \str_replace('tcp', 'http', $socket->getAddress()),
        ),
      );
      
      // log server errors to the console (discretionary)
      // $server->on('error', function (\Throwable $err) use ($writable) {
      // });

      return $server;
    },
  );
}

const socketServer = __NAMESPACE__ . '\\socketServer';

/**
 * loadConfig
 * extracts the contents of an .env file into an array
 *
 * loadConfig :: String -> Array
 * 
 * @param string $path
 * @return array
 */
function loadConfig(string $path): array
{
  // use function to handle configuration-related errors/exceptions
  $load = f\toException(
    // extract configuration from a file - into an array
    fn () => Dotenv::createImmutable($path)->load(),
    // otherwise, return an empty array
    fn ($_) => [],
  );

  return $load();
}

const loadConfig = __NAMESPACE__ . '\\loadConfig';
