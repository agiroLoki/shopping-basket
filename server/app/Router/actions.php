<?php

/**
 * Router/actions.php
 * 
 * Contains useful primitives for routing actions
 * -> provisioning a routing table
 * -> splitting and extracting request URI path fragments
 * 
 * @author Lochemem Bruno Michael <lochbm@gmail.com>
 */

declare(strict_types=1);

namespace App\Router;

use App\Http;
use \Chemem\Bingo\Functional\{
  Algorithms as f,
  PatternMatching as p,
};
use \React\{
  Promise\Promise,
  Http\Message\Response,
  EventLoop\LoopInterface,
  Promise\PromiseInterface,
};
use \Psr\Http\Message\ServerRequestInterface as Request;

/**
 * router
 * creates application router
 * 
 * router :: Object -> Object -> Array -> Promise s a
 *
 * @param Request $request Server request object
 * @param LoopInterface $loop event loop instance
 * @param array $config .env configuration
 * @return PromiseInterface
 * @example
 * 
 * router(new ServerRequest('GET', 'http://localhost:5000/'), $loop, [])
 * //=> object(React\Promise\Promise) {}
 */
function router(
  Request $request,
  LoopInterface $loop,
  array $config = []
): PromiseInterface {
  $routes = f\compose(
    // split the path by / grapheme
    splitPath,
    // define routing table
    f\partialRight(defineRoutes, $config, $loop, $request),
  );

  return $routes(
    $request->getUri()->getPath(),
  );
}

const router = __NAMESPACE__ . '\\router';

/**
 * splitPath
 * tokenizes URI path fragment
 *
 * splitPath :: String -> Array
 * 
 * @param string $path
 * @return array
 * @example
 * 
 * splitPath('/hello/world');
 * //=> ['hello', 'world']
 */
function splitPath(string $path): array
{
  $split = f\compose(
    // truncate a portion of a string by removing the first element
    // which is, for path data, the grapheme /
    fn (string $path) => f\startsWith($path, '/') ? \substr($path, 1) : $path,
    // separate the resultant path into an array of strings
    f\partial('explode', '/'),
  );

  return $split($path);
}

const splitPath = __NAMESPACE__ . '\\splitPath';
