<?php

/**
 * Auth/actions.php
 * 
 * Contains actions for user-identity-related actions
 * -> JWT encoding and decoding
 * -> primitives for validating user credentials and auth tokens
 * 
 * @author Lochemem Bruno Michael <lochbm@gmail.com>
 */

declare(strict_types=1);

namespace App\Http\Auth;

use App\Http;
use \Firebase\JWT\JWT;
use \Chemem\Bingo\Functional\{
  Algorithms as f,
  Functors\Maybe
};

/**
 * jwtEncode
 * creates a JWT from an arbitrary set of claims
 * -> defaults to the HS256 algorithm
 * 
 * jwtEncode :: Array -> String
 * 
 * @param array $claims
 * @return string
 * @example
 * 
 * jwtEncode('@secret', ['handle' => '@ace411'])
 * //=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJoYW5kbG...'
 */
function jwtEncode(string $secret, array $claims = []): string
{
  // add claims to default set
  $payload = f\extend(Http\JWT_BASE_CLAIMS, $claims);
  
  // generate JWT from list of claims
  $encode  = f\toException(
    fn (string $secret) => JWT::encode($payload, $secret),
    fn ($_)             => '',
  );

  return $encode($secret);
}

const jwtEncode = __NAMESPACE__ . '\\jwtEncode';

/**
 * jwtDecode
 * decodes JWT into an array of claims
 * -> defaults to the HS256 algorithm
 *
 * jwtDecode :: String -> Array
 * 
 * @param string $token
 * @return array
 * @example
 * 
 * jwtDecode('eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJoYW5kbG...')
 * //=> [
 *  'iss'     => 'madewithlove-project',
 *  'aud'     => 'madewithlove-project',
 *  'handle'  => '@ace411',
 * ]
 */
function jwtDecode(string $secret, string $token): array
{
  $decode = f\toException(
    fn (string $token) =>
      (array) JWT::decode($token, $secret, ['HS256']),
    fn ($_) => [],
  );

  return $decode($token);
}

const jwtDecode = __NAMESPACE__ . '\\jwtDecode';

/**
 * filterCredentials
 * validates user credentials sent to /login endpoint
 * -> uses the Maybe monad
 * -> checks if username exists
 * -> compares given password with stored hash
 * -> returns an empty array in the event of failure
 *
 * filterCredentials :: Array -> Array -> Array
 * 
 * @param array $body
 * @param array $dbentries
 * @return array
 * @example
 * 
 * filterCredentials(
 *  [
 *    'username' => 'sales',
 *    'password' => 'sl_data',
 *  ],
 *  [
 *    [
 *      'username' => 'sales',
 *      'password' => '$argon2i$v=19$m=65536,t=4,p=1$Di...',
 *    ],
 *  ],
 * )
 * //=> []
 */
function filterCredentials(array $body, array $dbentries): array
{
  return Maybe\maybe(
    [],
    f\identity,
    Maybe\Maybe::just($body)
      // check if username and password fields exist
      ->filter(
        f\partialRight(f\keysExist, 'username', 'password'),
      )
      // check if user exists and password is valid effectively authenticating user
      ->filter(
        function (array $data) use ($dbentries) {
          $pluck        = f\partialRight(f\pluck, 'password');

          // extract credentials that match given username
          $credentials  = f\where($dbentries, f\addKeys($data, 'username'));

          // return false - effectively invalidating credentials if username is fictitious
          if (empty($credentials)) {
            return false;
          }

          // verify that given password matches stored hash
          return \password_verify(
            $pluck($data),
            $pluck(f\head($credentials)),
          );
        },
      ),
  );
}

const filterCredentials = __NAMESPACE__ . '\\filterCredentials';

/**
 * filterAuthToken
 * validates token for login-predicated actions
 * -> uses Maybe monad case-analysis
 * -> uses issuance secret to verify token payload
 * 
 * filterAuthToken :: String -> String -> Array
 * 
 * @param string $token
 * @param string $secret
 * @return array
 * @example
 * 
 * filterAuthToken('', '@secret')
 * //=> []
 */
function filterAuthToken(string $token, string $secret): array
{
  $maybe = f\compose(
    f\partial(jwtDecode, $secret),
    Maybe\Maybe::just,
  );

  return Maybe\maybe(
    [],
    f\identity,
    $maybe($token)
      // check if payload contains iss, aud, and username claims
      ->filter(
        f\partialRight(f\keysExist, 'iss', 'aud', 'username'),
      )
      // check if iss and aud claims match the defaults
      // verify username claim is string
      ->filter(
        fn ($claims) =>
          f\addKeys($claims, 'iss', 'aud') === Http\JWT_BASE_CLAIMS &&
            \is_string(f\pluck($claims, 'username')),
      )
  );
}

const filterAuthToken = __NAMESPACE__ . '\\filterAuthToken';
