<?php

/**
 * Auth/controllers.php
 * 
 * Contains controllers that operationalize user-identity-related endpoints
 * -> logins
 * -> cart creation
 * -> conveyance of items flagged for potential discount
 * 
 * @author Lochemem Bruno Michael <lochbm@gmail.com>
 */

declare(strict_types=1);

namespace App\Http\Auth;

use \React\{
  EventLoop\LoopInterface,
  Promise\PromiseInterface,
  Promise\Promise,
};
use function App\{
  Http\httpResponse,
  Filesystem\databasePath,
  Filesystem\fileRead,
};
use \Chemem\Bingo\Functional\Algorithms as f;
use \Psr\Http\Message\ServerRequestInterface as Request;

/**
 * login
 * logs user in effectively creating a unique JWT to authenticate them
 * 
 * login :: Object -> Array -> Array -> Promise s a
 *
 * @param LoopInterface $loop
 * @param array $entities
 * @param array $config
 * @return PromiseInterface
 * @example
 * 
 * login($loop, $request, [
 *  'USERS_DB'    => 'users.json',
 *  'JWT_SECRET'  => '@secret'
 * ])
 * //=> object(React\Promise\Promise) {}
 */
function login(
  LoopInterface $loop,
  Request $request,
  array $config
): PromiseInterface {
  $users = databasePath('USERS_DB', 'users.json', $config);
  
  // check if the HTTP method is valid
  return $request->getMethod() === 'POST' ?
    // check if the credentials are valid
    fileRead($loop, $users)
      ->then(
        function ($contents) use ($request, $config) {
          $body = $request->getParsedBody() ?? [];
          // validate request body
          return filterCredentials($body, \json_decode($contents, true)) ?
            // generate a JWT if the credentials are valid
            httpResponse(200, [
              'token' => jwtEncode(
                // extract secret key from .env configuration
                f\pluck($config, 'JWT_SECRET'),
                // add username to JWT payload
                f\addKeys($body, 'username'),
              ),
            ]) :
            // return a 400 response if the credentials are invalid
            httpResponse(400, ['error' => 'Invalid request body']);
        },
      ) :
    httpResponse(405, ['error' => 'Invalid method']);
}

const login = __NAMESPACE__ . '\\login';

/**
 * cartCreate
 * creates a JWT with cart information
 * -> creates a unique cart id
 * -> creates an empty cart
 * 
 * cartCreate :: Object -> Object -> Promise s a
 *
 * @param LoopInterface $loop
 * @param Request $request
 * @return PromiseInterface
 * @example
 * 
 * cartCreate($loop, $request, ['JWT_SECRET' => '@secret'])
 * //=> object(React\Promise\Promise) {}
 */
function cartCreate(
  LoopInterface $loop,
  Request $request,
  array $config
): PromiseInterface {
  // generate secure pseudo random identifier for each cart
  // -> token is storable in local storage and usable in Cookie header
  // -> akin to using sessions
  $bytes = \openssl_random_pseudo_bytes(5);
  
  return $request->getMethod() === 'GET' ?
    httpResponse(201, ['ok' => true], [
      'Set-Cookie' => f\concat(
        '=',
        'cart',
        jwtEncode(
          f\pluck($config, 'JWT_SECRET', ''),
          [
            'cartid'  => \bin2hex($bytes),
            'cart'    => [],
          ],
        ),
      ),
    ]) :
    httpResponse(405, ['error' => 'Invalid method']);
}

const cartCreate = __NAMESPACE__ . '\\cartCreate';

/**
 * getFlaggedProducts
 * access data in store containing items flagged as fit for discount
 * -> avails items removed from carts - and placed in store - to valid users
 * 
 * getFlaggedProducts :: Object -> Object -> Array -> Promise s a
 *
 * @param LoopInterface $loop
 * @param Request $request
 * @param array $config
 * @return PromiseInterface
 */
function getFlaggedProducts(
  LoopInterface $loop,
  Request $request,
  array $config
): PromiseInterface {
  // get authorization header
  // extract token and validate it
  // return contents of discounts db
  $token = filterAuthToken(
    // browser representation of authorization header might be lowercase
    $request->getHeaderLine('authorization') ??
      $request->getHeaderLine('Authorization') ??
        '',
    f\pluck($config, 'JWT_SECRET', ''),
  );

  // group all flagged products by name
  // -> ['product' => [orders]]
  $group = f\compose(
    f\partialRight('json_decode', true),
    f\partialRight(f\groupBy, 'name'),
  );

  return $request->getMethod() === 'GET' ?
    !empty($token) ?
      fileRead(
        $loop,
        databasePath(
          f\pluck($config, 'DISCOUNTS_DB'),
          'discounts.json',
          $config,
        ),
      )->then(
        fn ($items) => httpResponse(200, $group($items)),
      ) :
      httpResponse(401, ['error' => 'Unauthorized']) :
    httpResponse(405, ['error' => 'Invalid method']);
}

const getFlaggedProducts = __NAMESPACE__ . '\\getFlaggedProducts';
