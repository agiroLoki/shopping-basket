<?php

/**
 * Http/functions.php
 * 
 * Contains artifacts useful in completion of HTTP-related actions
 * -> server responses and the like
 * 
 * @author Lochemem Bruno Michael <lochbm@gmail.com>
 */

declare(strict_types=1);

namespace App\Http;

use \React\{
  Http\Message\Response,
  Promise\PromiseInterface,
};
use Chemem\Bingo\Functional\Algorithms as f;
use function \React\Promise\resolve;

/**
 * httpResponse
 * subsumes HTTP response returned by server in a Promise
 *
 * httpResponse :: Int -> Array -> Bool -> Array -> Promise s a
 * 
 * @param integer $code
 * @param array $msg
 * @param boolean $resolve
 * @param array $headers
 * @return PromiseInterface
 * @example
 * 
 * httpResponse(201, ['message' => 'Value created'])
 * //=> object(React\Promise\Promise) {}
 */
function httpResponse(
  int $code,
  array $msg,
  array $headers = []
): PromiseInterface {
  $response = new Response(
    // HTTP response code (200, 201, 401, 400 ...etc)
    $code,
    // multiple HTTP headers (CORS + JSON type specification + arbitrary headers)
    f\extend(
      JSON_TYPE,
      CORS_TYPE,
      HTTP_SERVER_NAME,
      NOSNIFF_HEADER,
      $headers,
    ),
    // print neat, multi-line JSON response
    \json_encode($msg, JSON_PRETTY_PRINT),
  );

  return resolve($response);
}

const httpResponse = __NAMESPACE__ . '\\httpResponse';
