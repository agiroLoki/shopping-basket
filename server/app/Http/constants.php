<?php

/**
 * Http/constants.php
 * 
 * Contains useful data for completion of HTTP-related actions
 * 
 * @author Lochemem Bruno Michael <lochbm@gmail.com>
 */

declare(strict_types=1);

namespace App\Http;

/**
 * @var JSON_TYPE
 * 
 * JSON Content-Type header
 */
const JSON_TYPE = [
  'Content-Type' => 'application/json',
];

/**
 * @var CORS_TYPE
 * 
 * CORS settings for cross-domain requests
 */
const CORS_TYPE = [
  'Access-Control-Allow-Origin'   => '*',
  'Access-Control-Max-Age'        => 1728000,
  'Access-Control-Allow-Methods'  => 'POST, GET, OPTIONS',
  'Access-Control-Allow-Headers'  => 'DNT, Content-Type, Cache-Control, Keep-Alive, Referer, X-Requested-With, Accept, User-Agent, X-Custom-Header, If-Modified-Since, Content-Range, Range, Origin, Host',
];

/**
 * @var JWT_BASE_CLAIMS
 * 
 * Default claims for JWT encoding/decoding
 */
const JWT_BASE_CLAIMS = [
  'iss' => 'madewithlove-project',
  'aud' => 'madewithlove-project',
];

/**
 * @var HTTP_SERVER_NAME
 * 
 * App server name
 */
const HTTP_SERVER_NAME = [
  'Server' => 'MadewithLove-Project',
];

/**
 * @var NOSNIFF_HEADER
 * 
 * Preempts MIME type sniffing
 * @see https://docs.sucuri.net/warnings/hardening/security-headers-x-content-type-nosniff/
 */
const NOSNIFF_HEADER = [
  'X-Content-Type-Options' => 'nosniff',
];
