<?php

/**
 * Products/controllers.php
 * 
 * Contains controllers that operationalize product and cart/basket-related endpoints
 * -> cart modifications
 * -> product listings
 * -> checkouts
 * 
 * @author Lochemem Bruno Michael <lochbm@gmail.com>
 */

declare(strict_types=1);

namespace App\Http\Products;

use \React\{
  EventLoop\LoopInterface,
  Promise\PromiseInterface,
  Promise\Promise,
};
use App\Filesystem as fsys;
use function App\{
  Http\httpResponse,
  Filesystem\databasePath,
  Filesystem\fileAppend,
  Http\Auth\jwtEncode,
};
use \Chemem\Bingo\Functional\Algorithms as f;
use \Psr\Http\Message\ServerRequestInterface as Request;

/**
 * getProducts
 * prints list of products
 * 
 * getProducts :: Object -> Object -> Array -> Promise s a
 * 
 * @param LoopInterface $loop
 * @param Request $request
 * @param array $config
 * @return PromiseInterface
 * @example
 * 
 * getProducts($loop, $request, ['PRODUCTS_DB' => 'products.json'])
 * //=> object(React\Promise\Promise) {}
 */
function getProducts(
  LoopInterface $loop,
  Request $request,
  array $config
): PromiseInterface {
  return $request->getMethod() === 'GET' ?
    // read file contents
    fsys\fileRead(
      $loop,
      databasePath('PRODUCTS_DB', 'products.json', $config),
    )->then(
      fn (string $contents) =>
        httpResponse(200, \json_decode($contents, true)),
    ) :
    // print a 405 Invalid Header response
    httpResponse(405, ['error' => 'Invalid method']);
}

const getProducts = __NAMESPACE__ . '\\getProducts';

/**
 * addToCart
 * adds items to a cart
 * 
 * addToCart :: Object -> Object -> Array -> Promise s a
 *
 * @param LoopInterface $loop
 * @param Request $request
 * @param array $config
 * @return PromiseInterface
 * @example
 * 
 * addToCart($loop, $request, [])
 * //=> object(React\Promise\Promise) {}
 */
function addToCart(
  LoopInterface $loop,
  Request $request,
  array $config
): PromiseInterface {
  return modifyCart(
    $loop,
    $request,
    $config,
    fn (array $claims, array $checkout) =>
      // check if checkout data is valid
      !empty($checkout) ?
        httpResponse(
          200,
          // extract cart from claims
          // -> preempts including unnecessary JWT information to client
          f\addKeys(pushToCartPayload($claims, $checkout), 'cart'),
          // put modified cart token in cookie
          // -> allows for use in subsequent modification calls
          [
            'Set-Cookie' => f\concat(
              '=',
              'cart',
              jwtEncode(
                f\pluck($config, 'JWT_SECRET'),
                pushToCartPayload($claims, $checkout),
              ),
            ),
          ]
        ) :
        // return a 400 invalid data response
        httpResponse(400, ['error' => 'Invalid request data']),
  );
}

const addToCart = __NAMESPACE__ . '\\addToCart';

/**
 * removeFromCart
 * removes item from a cart
 *
 * removeFromCart :: Object -> Object -> Array -> Promise s a
 * 
 * @param LoopInterface $loop
 * @param Request $request
 * @param array $config
 * @return PromiseInterface
 * @example
 * 
 * removeFromCart($loop, $request, [])
 * //=> object(React\Promise\Promise) {}
 */
function removeFromCart(
  LoopInterface $loop,
  Request $request,
  array $config
): PromiseInterface {
  return modifyCart(
    $loop,
    $request,
    $config,
    function (array $claims, array $checkout) use ($loop, $config) {
      // write function for appending to file
      // -> converts contents to json before merging and eventually, writing
      $writefn = f\compose(
        f\partialRight('json_decode', true),
        f\partial(f\extend, [$checkout]),
        fsys\jsonEncode,
      );
      
      return !empty($checkout) ?
        (
          // check the item in the request body (checkout data) matches that in cart
          // -> preempts deletion of items that don't exist in the cart
          f\any(
            f\pluck($claims, 'cart'),
            fn ($entry) => (array) $entry === $checkout,
          ) ?
            // write data popped from cart to file
            // -> in accordance with question
            fileAppend(
              $loop,
              databasePath('DISCOUNTS_DB', 'discounts.json', $config),
              $writefn,
            )->then(
              fn () =>
                httpResponse(
                  200,
                  // return cart data to show completion of task
                  f\addKeys(popFromCartPayload($claims, $checkout), 'cart'),
                  // update cookie to reflect changes
                  [
                    'Set-Cookie' => f\concat(
                      '=',
                      'cart',
                      jwtEncode(
                        f\pluck($config, 'JWT_SECRET'),
                        popFromCartPayload($claims, $checkout),
                      ),
                    ),
                  ]
                ),
            ) :
            // return a 400 item not in cart error
            httpResponse(400, ['error' => 'Item is not in cart'])
        ) :
        // return a 400 invalid request data error
        httpResponse(400, ['error' => 'Invalid request data']);
    },
  );
}

const removeFromCart = __NAMESPACE__ . '\\removeFromCart';

/**
 * checkout
 * completes checkout action
 * -> resets cart
 * -> does not write to any data store
 * 
 * checkout :: Object -> Object -> Array -> Promise s a
 * 
 * @param LoopInterface $loop
 * @param Request $request
 * @param array $config
 * @return PromiseInterface
 * @example
 * 
 * checkout($loop, $request, [])
 * //=> object(React\Promise\Promise) {}
 */
function checkout(
  LoopInterface $loop,
  Request $request,
  array $config
): PromiseInterface {
  // get cart from request
  $cart = f\pluck($request->getCookieParams() ?? [], 'cart', '');
  // get JWT secret from configuration
  $secret = f\pluck($config, 'JWT_SECRET', '');
  // validate cart - thereby extracting cart token payload
  $claims = filterCartToken($cart, $secret);
  
  return $request->getMethod() === 'GET' ?
    (
      !empty($claims) ?
        httpResponse(200, ['ok' => true], [
          // reset cart upon successful checkout
          'Set-Cookie' => f\concat(
            '=',
            'cart',
            jwtEncode($secret, f\extend($claims, ['cart' => []])),
          ),
        ]) :
        httpResponse(400, ['error' => 'Cart is invalid'])
    ) :
    httpResponse(405, ['error' => 'Invalid method']);
}

const checkout = __NAMESPACE__ . '\\checkout';
