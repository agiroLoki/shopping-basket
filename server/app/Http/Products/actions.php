<?php

/**
 * Products/actions.php
 * 
 * Contains actions for product and cart/basket-related actions
 * -> primitives for validating cart and checkout information
 * -> a template for cart modification
 * 
 * @author Lochemem Bruno Michael <lochbm@gmail.com>
 */

declare(strict_types=1);

namespace App\Http\Products;

use \Chemem\Bingo\Functional\{
  Functors\Maybe,
  Algorithms as f,
};
use App\{Http, Http\Auth};
use \React\{
  Promise\Promise,
  Http\Message\Response,
  EventLoop\LoopInterface,
  Promise\PromiseInterface,
};
use function App\{
  Http\httpResponse,
  Filesystem\fileRead,
  Filesystem\databasePath,
  Http\Auth\jwtEncode,
};
use function \React\Promise\resolve;
use \Psr\Http\Message\ServerRequestInterface as Request;

/**
 * filterCheckoutData
 * validates checkout data
 * -> uses the Maybe monad in place of an exception
 * -> compares arbitrary data with database entries
 * -> returns an empty array if the data is invalid
 *
 * filterCheckoutData :: Array -> Array -> Array
 * 
 * @param array $data
 * @param array $dbentries
 * @return array
 * @example
 * 
 * filterCheckoutData(
 *  [
 *    'name'      => 'baz',
 *    'quantity'  => 11,
 *  ],
 *  [
 *    ['name' => 'foo', 'price' => 15],
 *    ['name' => 'bar', 'price' => 12],
 *  ],
 * )
 * //=> []
 */
function filterCheckoutData(array $data, array $products): array
{
  //function to extract name from list
  $name = f\partialRight(f\pluck, 'name');

  // function to compute item cost
  $cost = fn ($body) =>
    f\compose(
      // find the item in the product list that matches the name of the entry
      f\partial(f\filter, fn ($entry) => $name($body) == $name($entry)),
      // extract its first element
      f\head,
      // compute the cost and append it to the body data
      fn (array $entry) =>
        f\extend($data, [
          'cost' => f\pluck($entry, 'price') * f\pluck($data, 'quantity'),
        ])
    );

  return Maybe\maybe(
    [],
    fn ($checkout) => $cost($checkout)($products),
    Maybe\Maybe::just($data)
      // check if name, quantity, and cost fields exist
      ->filter(
        f\partialRight(f\keysExist, 'name', 'quantity'),
      )
      // validate name and quantity fields
      ->filter(
        function (array $data): bool {
          $pluck    = f\partial(f\pluck, $data);
          $isNumber = fn ($entry): bool =>
            \is_numeric($entry) || \is_float($entry) || \is_int($entry);

          return $isNumber($pluck('quantity')) &&
            $pluck('quantity') !== 0 &&
            \is_string($pluck('name'));
        },
      )
      // check if product exists
      ->filter(
        fn (array $body): bool =>
          f\any($products, fn ($entry) => $name($entry) === $name($body)),
      ),
  );
}

const filterCheckoutData = __NAMESPACE__ . '\\filterCheckoutData';

/**
 * filterCartToken
 * validates cart JWT
 * -> uses Maybe monad case analysis
 * -> uses issuance secret to verify token payload
 *
 * @param string $token
 * @param string $secret
 * @return array
 * @example
 * 
 * filterCartToken('', '@secret')
 * // => []
 */
function filterCartToken(string $token, string $secret): array
{
  $maybe = f\compose(
    f\partial(Auth\jwtDecode, $secret),
    Maybe\Maybe::just,
  );
  
  return Maybe\maybe(
    [],
    f\identity,
    $maybe($token)
      // check if the payload contains iss, aud, cart, and cartid claims
      ->filter(
        f\partialRight(f\keysExist, 'iss', 'aud', 'cart', 'cartid'),
      )
      // check if iss and aud claims match the defaults
      // verify cart properties
      // -> check if cartid length is 10
      // -> check if the cart is an actual array
      ->filter(
        function (array $claims) {
          $pluck = f\partial(f\pluck, $claims);

          return f\addKeys($claims, 'iss', 'aud') === Http\JWT_BASE_CLAIMS &&
              \strlen($pluck('cartid')) === 10 &&
              \is_array($pluck('cart'));
        },
      )
  );
}

const filterCartToken = __NAMESPACE__ . '\\filterCartToken';

/**
 * popFromCartPayload
 * removes checkout data from cart JWT payload
 *
 * pushToCartPayload :: Array -> Array -> Array
 * 
 * @param array $payload
 * @param array $body
 * @return array
 */
function popFromCartPayload(array $payload, array $body): array
{
  $pop = f\compose(
    // get the cart from the payload
    f\partialRight(f\pluck, [], 'cart'),
    // remove item that matches body data
    f\partial(f\reject, fn ($entry) => (array) $entry === $body),
    // reset list keys
    'array_values',
  );

  return f\extend($payload, ['cart' => $pop($payload)]);
}

const popFromCartPayload = __NAMESPACE__ . '\\popFromCartPayload';

/**
 * pushToCartPayload
 * adds checkout data to cart JWT payload
 *
 * pushToCartPayload :: Array -> Array -> Array
 * 
 * @param array $payload
 * @param array $body
 * @return array
 */
function pushToCartPayload(array $payload, array $body): array
{
  $modify = f\compose(
    // get the cart from the payload
    f\partialRight(f\pluck, [], 'cart'),
    // append the body contents to payload cart
    f\partial(f\extend, [$body]),
  );

  return f\extend($payload, ['cart' => $modify($payload)]);
}

const pushToCartPayload = __NAMESPACE__ . '\\pushToCartPayload';

/**
 * modifyCart
 * a primer for cart modifications - actions that alter cart internal state
 * -> filters cart token available in cookie
 * -> passes modification control to arbitrary handler
 *
 * modifyCart :: Object -> Object -> Array -> (Array -> Array -> Promise s a) -> Promise s a
 * 
 * @param LoopInterface $loop
 * @param Request $request
 * @param array $config
 * @param callable $handler
 * @return PromiseInterface
 */
function modifyCart(
  LoopInterface $loop,
  Request $request,
  array $config,
  callable $handler
): PromiseInterface {
  // get cart from list of cookie parameters
  $cart   = f\pluck($request->getCookieParams() ?? [], 'cart', '');
  // get user-supplied cart data from request body
  $body   = $request->getParsedBody() ?? [];
  // validate cart and thus extract cart token claims
  $claims = filterCartToken($cart, f\pluck($config, 'JWT_SECRET', ''));

  return $request->getMethod() === 'POST' ?
    (
      // check if cart token is valid
      !empty($claims) ?
        fileRead(
          $loop,
          databasePath('PRODUCTS_DB', 'products.json', $config),
        )->then(
          fn (string $products) =>
            // pass claims to discretionary handler which accepts checkout data in body
            resolve(
              filterCheckoutData($body, \json_decode($products, true)),
            )->then(f\partial($handler, $claims)),
        ) :
        // return a 400 invalid cart error
        httpResponse(400, ['error' => 'Cart is invalid'])
    ) :
    // return a 405 invalid method error
    httpResponse(405, ['error' => 'Invalid method']);
}

const modifyCart = __NAMESPACE__ . '\\modifyCart';
