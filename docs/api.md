# API Documentation

This document contains information about the core API's endpoints - and describes each resource and resource-related entity.

**NOTE:** The data used in the requests and responses are usable with the server and are not just placeholders.

## `/api/products`

**`GET /api/products`**

Returns a list of all available store products.

**Request Headers:**

- Content-Type -
  - application/json

**Response Headers:**

- Content-Type -
  - application/json

**Response JSON Object**:

- **products** (array) - An array of available products
  - **name** (string) - Product name
  - **price** (float) - Product price
- **error** (string) - Error message (conditional)

**Status Codes:**

- 200 OK - Request was completed successfully
- 405 Invalid method - The wrong HTTP method was used when making the request

**Request**

```
GET /api/products HTTP/1.1
Content-Type: application/json
```

**Response**

```
HTTP/1.1 200 OK
Content-Type: application/json
Content-Length: 360

[
	{
		"name": "Pioneer DJ Mixer",
		"price": 699
	},
	{
		"name": "Roland Wave Sampler",
		"price": 485
	},
	{
		"name": "Reloop Headphone",
		"price": 159
	},
	{
		"name": "Rokit Monitor",
		"price": 189.9
	},
	{
		"name": "Fisherprice Baby Mixer",
		"price": 120
	}
]
```

## `/api/cart/create`

**`GET /api/cart/create`**

Creates a unique cart subsumed in the body of a JWT. The said cart is subject to modification.

**Request Headers**:

- Content-Type -
  - application/json

**Response Headers**:

- Content-Type -
  - application/json
- Set-Cookie -
  - cart={token}

**Non-JSON Response entity(ies)**:

- **cart** (string) - A JWT containing a set of cart-specific claims
  - **iss** (string) - Token Issuer
  - **aud** (string) - Token Audience
  - **cartid** (string) - Unique cart identifier
  - **cart** (array) - Modifiable shopping basket

**Response JSON Object**:

- **ok** (boolean) - Action completion confirmation
- **error** (string) - Error message (conditional)

**Status Codes**:

- 201 Created - Cart was successfully created
- 405 Invalid method - The wrong HTTP method was used when making the request

**Request**

```
GET /api/cart/create HTTP/1.1
Content-Type: application/json
```

**Response**

```
HTTP/1.1 201 Created
Content-Type: application/json
Set-Cookie: cart=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJtYWRld2l0aGxvdmUtcHJvamVjdCIsImF1ZCI6Im1hZGV3aXRobG92ZS1wcm9qZWN0IiwiY2FydGlkIjoiNDgxYzE1MDFkMiIsImNhcnQiOltdfQ.WNdmPZ3R6ou0KL2exQkuIW9Ly1NeO4VLg4lD_29D7kM
Content-Length: 18

{
  "ok": true
}
```

## `/api/cart/add`

**`POST /api/cart/add`**

Adds item to cart. Returns a key-value representation of the JWT-subsumed cart and Cookie value - storable in an intermediate datastore - in an aptly named header.

**Request Headers**:

- Content-Type -
  - application/json
- Cookie -
  - cart={token}

**Request JSON Object**:

- **name** (string) - Name of product to add to cart (should exist in product store)
- **quantity** (integer) - Arbitrary quantity of specified product

**Response Headers**:

- Content-Type -
  - application/json
- Set-Cookie -
  - cart={token}

**Non-JSON Response entity(ies)**:

- **cart** (string) - A JWT containing a set of cart-specific claims
  - **iss** (string) - Issuer
  - **aud** (string) - Audience
  - **cartid** (string) - Unique cart identifier
  - **cart** (array) - Modifiable shopping basket
- **error** (string) - Error message (conditional)

**Response JSON Object**:

- **cart** (array) - An array of JSON items present in the products store
  - **name** (string) - Name of product
  - **quantity** (integer) - Quantity of product
  - **cost** (float) - Total cost of product

**Status Codes**:

- 200 OK - Request was completed successfully
- 400
  - Cart is Invalid - JWT is unverifiable
  - Invalid request data - Request body contains items that do not exist in the product store
- 405 Invalid method - The wrong HTTP method was used when making the request

**Request**

```
POST /api/cart/add HTTP/1.1
Content-Type: application/json
Cookie: cart=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJtYWRld2l0aGxvdmUtcHJvamVjdCIsImF1ZCI6Im1hZGV3aXRobG92ZS1wcm9qZWN0IiwiY2FydGlkIjoiNDgxYzE1MDFkMiIsImNhcnQiOltdfQ.WNdmPZ3R6ou0KL2exQkuIW9Ly1NeO4VLg4lD_29D7kM

{
  "name": "Rokit Monitor",
  "quantity": 3
}
```

**Response**

```
HTTP/1.1 200 OK
Content-Type: application/json
Set-Cookie: cart=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJtYWRld2l0aGxvdmUtcHJvamVjdCIsImF1ZCI6Im1hZGV3aXRobG92ZS1wcm9qZWN0IiwiY2FydGlkIjoiNDgxYzE1MDFkMiIsImNhcnQiOlt7Im5hbWUiOiJSb2tpdCBNb25pdG9yIiwicXVhbnRpdHkiOjMsImNvc3QiOjU2OS43fV19.ylQC1c3mKZUUWl3cp1-5FW_1n0N5LEJLZkLZ3w544jQ
Content-Length: 133

{
  "cart": [
    {
      "name": "Rokit Monitor",
      "quantity": 3,
      "cost": 569.7
    }
  ]
}
```

## `/api/cart/remove`

**`POST /api/cart/remove`**

Removes an item from a cart. It also returns a key-value representation of the JWT-subsumed cart and Cookie value - storable in an intermediate datastore - in an aptly named header.

**Request Headers**:

- Content-Type -
  - application/json
- Cookie -
  - cart={token}

**Request JSON Object**:

- **name** (string) - Name of product to remove from cart (should exist in product store)
- **quantity** (integer) - Arbitrary quantity of specified product

**Response Headers**:

- Content-Type -
  - application/json
- Set-Cookie -
  - cart={token}

**Non-JSON Response entity(ies)**:

- **cart** (string) - A JWT containing a set of cart-specific claims
  - **iss** (string) - Issuer
  - **aud** (string) - Audience
  - **cartid** (string) - Unique cart identifier
  - **cart** (array) - Modifiable shopping basket
- **error** (string) - Error message (conditional)

**Response JSON Object**:

- **cart** (array) - An array of JSON items present in the products store
  - **name** (string) - Name of product
  - **quantity** (integer) - Quantity of product
  - **cost** (float) - Total cost of product

**Status Codes**:

- 200 OK - Request was completed successfully
- 400
  - Cart is Invalid - JWT is unverifiable
  - Item is not in cart - Signals attempted deletion of an item that is not in cart
  - Invalid request data - Request body contains items that do not exist in the product store
- 405 Invalid method - The wrong HTTP method was used when making the request

**Request**

```
POST /api/cart/remove HTTP/1.1
Content-Type: application/json
Cookie: cart=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJtYWRld2l0aGxvdmUtcHJvamVjdCIsImF1ZCI6Im1hZGV3aXRobG92ZS1wcm9qZWN0IiwiY2FydGlkIjoiNDgxYzE1MDFkMiIsImNhcnQiOlt7Im5hbWUiOiJSZWxvb3AgSGVhZHBob25lIiwicXVhbnRpdHkiOjEwLCJjb3N0IjoxNTkwfSx7Im5hbWUiOiJSb2tpdCBNb25pdG9yIiwicXVhbnRpdHkiOjMsImNvc3QiOjU2OS43fV19.9fsSNXTzXU0b5VKVbke0j6D9STCrFy_u_xFiglwevdI

{
  "name": "Reloop Headphone",
  "quantity": 10
}
```

**Response**

```
HTTP/1.1 200 OK
Content-Type: application/json
Set-Cookie: cart=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJtYWRld2l0aGxvdmUtcHJvamVjdCIsImF1ZCI6Im1hZGV3aXRobG92ZS1wcm9qZWN0IiwiY2FydGlkIjoiNDgxYzE1MDFkMiIsImNhcnQiOlt7Im5hbWUiOiJSb2tpdCBNb25pdG9yIiwicXVhbnRpdHkiOjMsImNvc3QiOjU2OS43fV19.ylQC1c3mKZUUWl3cp1-5FW_1n0N5LEJLZkLZ3w544jQ
Content-Length: 133

{
  "cart": [
    {
      "name": "Rokit Monitor",
      "quantity": 3,
      "cost": 569.7
    }
  ]
}
```

## `/api/checkout`

**`GET /checkout`**

Completes checkout process and effectively resets the cart.

**Request Headers**:

- Content-Type -
  - application/json
- Cookie -
  - cart={token}

**Response Headers**:

- Content-Type -
  - application/json
- Set-Cookie -
  - cart={token}

**Non-JSON Response entity(ies)**:

- **cart** (string) - A JWT containing a set of cart-specific claims
  - **iss** (string) - Issuer
  - **aud** (string) - Audience
  - **cartid** (string) - Unique cart identifier
  - **cart** (array) - Modifiable shopping basket

**Response JSON Object**:

- **ok** (boolean) - Action completion confirmation
- **error** (string) - Error message (conditional)

**Status Codes**:

- 200 OK - Request was completed successfully
- 400 Cart is Invalid - JWT is unverifiable
- 405 Invalid method - The wrong HTTP method was used when making the request

**Request**

```
GET /api/checkout HTTP/1.1
Content-Type: application/json
Cookie: cart=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJtYWRld2l0aGxvdmUtcHJvamVjdCIsImF1ZCI6Im1hZGV3aXRobG92ZS1wcm9qZWN0IiwiY2FydGlkIjoiNDgxYzE1MDFkMiIsImNhcnQiOlt7Im5hbWUiOiJSb2tpdCBNb25pdG9yIiwicXVhbnRpdHkiOjMsImNvc3QiOjU2OS43fV19.ylQC1c3mKZUUWl3cp1-5FW_1n0N5LEJLZkLZ3w544jQ
```

**Response**

```
HTTP/1.1 200 OK
Content-Type: application/json
Set-Cookie: cart=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJtYWRld2l0aGxvdmUtcHJvamVjdCIsImF1ZCI6Im1hZGV3aXRobG92ZS1wcm9qZWN0IiwiY2FydGlkIjoiNDgxYzE1MDFkMiIsImNhcnQiOltdfQ.WNdmPZ3R6ou0KL2exQkuIW9Ly1NeO4VLg4lD_29D7kM
Content-Length: 18

{
	"ok": true
}
```

## `/api/login`

**`POST /api/login`**

Completes a login action by comparing credential input with user data in `users.json` datastore. Successful identity verification results in the issuance of a user-specific JWT.

**Request Headers**:

- Content-Type -
  - application/json

**Request JSON Object**:

- **username** (string) - Unique username
- **password** (integer) - Matching user password

**Response Headers**:

- Content-Type -
  - application/json

**Response JSON Object**:

- **token** (string) - A JWT containing user-specific claims
- **error** (string) - Error message (conditional)

**Status Codes**:

- 200 OK - Request was completed successfully
- 400 Invalid request body - Indicates unparsable request data (malformed query data) or credential invalidity
- 405 Invalid method - The wrong HTTP method was used when making the request

**Request**

```
POST /api/login HTTP/1.1
Content-Type: application/json

{
	"username": "sales",
	"password": "sales_dept"
}
```

**Response**

```
HTTP/1.1 200 OK
Content-Type: application/json
Content-Length: 204

{
	"token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJtYWRld2l0aGxvdmUtcHJvamVjdCIsImF1ZCI6Im1hZGV3aXRobG92ZS1wcm9qZWN0IiwidXNlcm5hbWUiOiJzYWxlcyJ9.2-FyOvqP1c9ZpL4cQXvkEsHvtWzRW5Bl6xTC0pu71og"
}
```

## `/api/flagged`

**`GET /api/flagged`**

Returns a list of items removed from a cart before checkout (pre-checkout) and thus, flagged for potential future discounts.

**Request Headers:**

- Content-Type -
  - application/json
- Authorization/authorization -
  - {token}

**Response Headers:**

- Content-Type -
  - application/json

**Response JSON Object:**

- **flagged** (array) - An array containing entries grouped by product name.
  - **({product name})** (string) - Product name
    - **name** (string) - Name of product
    - **quantity** (integer) - Quantity of product
    - **cost** (float) - Total cost of product
- **error** (string) - Error message (conditional)

**Status Codes:**

- 200 OK - Request was completed successfully
- 401 Unauthorized - Signals authentication failure
- 405 Invalid method - The wrong HTTP method was used when making the request

**Request**

```
GET /api/flagged HTTP/1.1
Content-Type: application/json
Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJtYWRld2l0aGxvdmUtcHJvamVjdCIsImF1ZCI6Im1hZGV3aXRobG92ZS1wcm9qZWN0IiwidXNlcm5hbWUiOiJzYWxlcyJ9.2-FyOvqP1c9ZpL4cQXvkEsHvtWzRW5Bl6xTC0pu71og
```

**Response**

```
HTTP/1.1 200 OK
Content-Type: application/json
Content-Length: 148

{
	"Reloop Headphone": [
		{
			"name": "Reloop Headphone",
			"quantity": 10,
      "cost": 1590
		}
	]
}
```
