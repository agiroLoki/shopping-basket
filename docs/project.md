# Introduction

This document contains information about the Shopping Basket Application that solves the problem described in the README.md file. It is a PHP syntax that melds asynchrony and Functional Programming and facilitates the creation and modification of a shopping cart (or basket) via JSON Web Tokens.

## Approach

The app uses JWTs for the interchange of transient information - in a manner akin to session usage. It transmits cart (basket) information via cookies and writes any pre-checkout removals to a flat-file datastore named `discounts.json`. Also, the app provides a means for access to the data stored in the aforestated file. It contains a login resource whose purpose is availing pre-checkout data to a Sales Department staffer upon successful authentication of a user-specific JWT.

### Features

- Cart (basket) creation
- Cart modification via one of either addition or removal
  - The latter writes data checkout information to flat-file
- Sales department login
- Conveyance of pre-checkout data upon successful login

Please peruse the server [API documentation](api.md) for more information on how to use the service.

## Salient Features

As mentioned in the antecedent text, the app combines constructs within PHP's technical jurisdiction. Present in the app are the ideas described below.

### Functional Programming

Functional Programming is a paradigm centered around building programs from pure functions. The Shopping Cart project combines algebras (Algebraic Data Types) - prominent in FP parlance - with pure helper functions to facilitate the operations described in previous sections. It features Maybe and Reader monads for error handling and propagating shared environment state, and Promises for subsuming actionable success and error chains.

### Asynchrony

Asynchrony is another paradigm - one that emphasizes the use of non-blocking I/O to eliminate wait-times between process executions. The app runs atop ReactPHP - a utility for enabling asynchrony available to the PHP userland. It features a server and filesystem-interaction abstractions packaged as non-blocking routines.

## Possible Improvements

The app, as presently constructed, is amenable to improvement. The following stratagems can suffice in this regard.

1. Install a more potent event loop such as ext-uv to realize speed and capacity improvements.
2. Migrate all data in flat-file stores to a more potent database to truncate memory strain.
3. Employ additional caching - internally via ReactPHP's cache or with an asynchronous client for a Redis key-value store.
4. Enforcing request throttling to reduce the likelihood of a successful DDoS attack.
5. Securing the app with TLS, thereby minimizing the risk of Man-in-the-Middle attacks.

## Requirements

- PHP 7.4 or greater
- ext-sodium
- Composer
- make
- Docker (optional)

## Installation and Usage

It is possible to install the project via Git - and doing so requires that you type the following in a console of your choosing.

```sh
git clone git clone https://agiroLoki@bitbucket.org/agiroLoki/shopping-basket.git
```

Packaged with the application is a Makefile that contains directives that abstract pertinent app-related tasks - starting the server, running tests, and the like. To follow is a detailed list of project-specific Makefile directives runnable via the make utility.

### `make server_install`

This directive installs server dependencies via Composer. I recommend that you run this directive upon successfully installing the project via Git.

### `make server_start`

Starts the development server, which asynchronously updates the app to match filesystem changes.

### `make server_test_deps`

Provisions server test dependencies. Please run this directive before proceding to run the app tests.

### `make server_test`

This directive runs app tests written atop PHPUnit.

### `make server_cs`

This directive applies code-style fixes in accordance with rules defined in a project-specific php-cs-fixer `.php_cs` configuration file.

### `make docker_build`

This directive triggers a Docker build operation: it creates a Docker image for the app and subsequently runs it in a Docker container.
