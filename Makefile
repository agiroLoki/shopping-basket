SHELL := /bin/bash
DIR := ${CURDIR}

DOCKER_PATH = /usr/bin/docker
PHPUNIT_PATH = $(DIR)/server/vendor/bin/phpunit
PHPCS_PATH = $(DIR)/server/vendor/bin/php-cs-fixer
PHPWTC_PATH = $(DIR)/server/vendor/bin/php-watcher
FSYSTESTS_PATH = $(DIR)/server/app/tests/Filesystem/files
COMPOSER_ERROR = Please install Composer

# install server dependencies
server_install:
	@if [ -a $(shell which composer) ]; then \
		cd server && composer update; \
	else \
		echo "${COMPOSER_ERROR}"; \
	fi

# updates server dependencies
server_update:
	@if [ -a $(shell which composer) ]; then \
		cd server && composer update --lock && composer normalize; \
	else \
		echo "${COMPOSER_ERROR}"; \
	fi

# provisions server test dependencies
server_test_deps:
	@if [ ! -d ${FSYSTESTS_PATH} ]; then \
		mkdir ${FSYSTESTS_PATH}; \
	fi
	@touch ${FSYSTESTS_PATH}/append.txt && echo "foo" > ${FSYSTESTS_PATH}/append.txt
	@touch ${FSYSTESTS_PATH}/read.txt && echo "TEST FILE" > ${FSYSTESTS_PATH}/read.txt
	@touch ${FSYSTESTS_PATH}/write.txt

# run server tests
server_test:
	@if [ -a ${PHPUNIT_PATH} ]; then \
		cd server && ${PHPUNIT_PATH} -c app/tests/phpunit.xml; \
	else \
		echo "Please install phpunit"; \
	fi

# apply code style fixes via php-cs-fixer
server_cs:
	@if [ -a ${PHPCS_PATH} ]; then \
		cd server && ${PHPCS_PATH} fix --config=.php_cs --diff --verbose --allow-risky=yes; \
	else \
		echo "Please install php-cs-fixer"; \
	fi

# run server separately
server_start:
	@if [ -a ${PHPWTC_PATH} ]; then \
		cd server && ${PHPWTC_PATH} server.php; \
	else \
		echo "Please install php-watcher"; \
	fi

# build Docker image
docker_build:
	@if [ -a ${DOCKER_PATH} ]; then \
		docker build -f Dockerfile -t mwl_server . && docker run mwl_server; \
	else \
		echo "Please install Docker"; \
	fi
